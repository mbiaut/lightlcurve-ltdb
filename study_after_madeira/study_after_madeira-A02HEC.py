from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle
import matplotlib.pyplot as plt
import numpy as np
import argparse
from array import array
import re
import mysql.connector

parent_dir = os.path.dirname(os.path.dirname(__file__))
lib_dir = os.path.join(parent_dir, 'lib')
sys.path.append(lib_dir)
from lib_ltdb_light import *

def get_list_mtx_echanged(part = "ALL") :
    # Get topologie information
    topo_connection = mysql.connector.connect(
        user = 'reader',
        password = 'monUser',
        host = 'dbod-eti-lar-mon.cern.ch',
        database = 'LTDB_TOPO',
        port = 5505
    )
    cursor = topo_connection.cursor()

    # Exemple d'exécution d'une requête SQL
    query = "SELECT ltdbName, mtxIds FROM MTx_Exchange ORDER BY date_of_exchange DESC;"
    if part == "ALL" :
        query = "SELECT ltdbName, mtxIds FROM MTx_Exchange ORDER BY date_of_exchange DESC;"
    elif part == "BARREL" :
        query = "SELECT ltdbName, mtxIds FROM MTx_Exchange WHERE ltdbName LIKE 'I%' OR ltdbName LIKE 'H%' ORDER BY date_of_exchange DESC;"
    elif part == "ENDCAP" :
        query = "SELECT ltdbName, mtxIds FROM MTx_Exchange WHERE ltdbName NOT LIKE 'I%' AND ltdbName NOT LIKE 'H%' ORDER BY date_of_exchange DESC;"
    cursor.execute(query)
    results = cursor.fetchall()

    mtx_exchanged = []
    for row in results:
        mtx_exchanged.append(row)

    # Fermeture de la connexion
    cursor.close()
    topo_connection.close()

    return mtx_exchanged


#function returning the list of ltdb fiber interesting us
def get_ltdb_fibers_exchanged_list(part = "ALL") :
    mtx_exchanged = get_list_mtx_echanged(part)

    list_mtx_obj = []
    for mtx in mtx_exchanged :
        list_mtx_obj.append(LtdbMtx(mtx[0], mtx[1]))

    liste_to_return = []
    for mtxobj in list_mtx_obj :
        liste_to_return.append(mtxobj.fiber1)
        liste_to_return.append(mtxobj.fiber2)

    return liste_to_return


def get_full_ltdb_barrel() :
    fullltdbname = ()
    fullltdb = []
    for name in fullltdbname :
        fullltdb.append(Ltdb(name))

    return []


def get_full_ltdb_endcap() :
    fullltdbname = ("A02HEC",)

    fullltdb = []
    for name in fullltdbname :
        fullltdb.append(Ltdb(name))

    return fullltdb


#function returning the list of ltdb fiber interesting us
def get_ltdb_fibers_list(part = "ALL") :
    fullltdb = []
    if part == "ALL" :
        fullltdb = get_full_ltdb_barrel()
        fullltdb.append(get_full_ltdb_endcap())
    elif part == "BARREL" :
        fullltdb = get_full_ltdb_barrel()
    elif part == "ENDCAP" :
        fullltdb = get_full_ltdb_endcap()

    liste_fiber = []
    
    for ltdb in fullltdb :
        ltdb.add_all_mtx()
        for mtx in ltdb.concerned_mtx :
            liste_fiber.append(mtx.fiber1)
            liste_fiber.append(mtx.fiber2)

    return liste_fiber


def main():
    # Global variable for script :
    Y_plot_label = "lightpower (uW)"
    datedebut = '2024/04/10 00:00:00'
    datefinale = '2024/12/31 00:00:00'

    barrelminactual = 250
    barrelregressionlim = 300
    endcapminactual = 250
    endcapregressionlim = 300

    # Fiber list to replace
    fibers_to_replace =[]

    # Get list of fiber to watch
    ltdb_fibers_list_barrel = get_ltdb_fibers_list("BARREL")
    ltdb_fibers_list_endcap = get_ltdb_fibers_list("ENDCAP")

    # Create the two plot, barrel and endcap
    barrelplot = MultiCurve("Barrel exchanged mtx fiber", Y_label = Y_plot_label)
    endcapplot = MultiCurve("EndCap exchanged mtx fiber", Y_label = Y_plot_label)

    # Connection to the database
    try:
        dbconnexion = cx_Oracle.connect("ATLAS_PVSS_READER", "PVSSRED4PRO", "ATONR_ADG")
    except cx_Oracle.DatabaseError as e:
        print(f"Database connection error: {e}")
        return

    # Create all curves
    for fiber in ltdb_fibers_list_barrel :
        print("barrel fiber : " + fiber.ltdb + " " + str(fiber.number))
        fiber.get_point(dbconnexion)
        if fiber.notempty() :
            fiber.filter_data()
            fiber.cut_at_date(datedebut)

            last_day_average = fiber.get_last_day_average()
            droite_regression = fiber.linear_regression(finaledate = datefinale)
            final_regression = droite_regression[len(droite_regression)-1][1]

            if last_day_average < barrelminactual or final_regression < barrelregressionlim :
                curvelabel = fiber.name+" : "+"{:.2f}".format(fiber.lightloss())
                newcurve = CurveOnPlot(curvelabel, fiber.get_daily_average_data(), linestyle='-')
                barrelplot.add_curve(newcurve)
                newcurve = CurveOnPlot(curvelabel, fiber.linear_regression(finaledate = datefinale), linestyle=':')
                barrelplot.add_curve(newcurve)
                print("new curve in barrel : " + curvelabel)
                fibers_to_replace.append(fiber)
                if last_day_average < barrelminactual :
                    fiber.comment = "current lightpower = " + str(last_day_average) + " < 250"
                elif final_regression < barrelregressionlim :
                    fiber.comment = "according to final regression"
                else :
                    fiber.comment = "c'est quoi ce bordel : " + str(last_day_average)+" < 250 or "+str(final_regression)+" < 300" 
    
    for fiber in ltdb_fibers_list_endcap :
        print("endcap fiber : " + fiber.ltdb + " " + str(fiber.number))
        fiber.get_point(dbconnexion)
        if fiber.notempty() :
            fiber.filter_data()
            fiber.cut_at_date(datedebut)

            last_day_average = fiber.get_last_day_average()
            droite_regression = fiber.linear_regression(finaledate = datefinale)
            final_regression = droite_regression[len(droite_regression)-1][1]

            if last_day_average < endcapminactual or final_regression < endcapregressionlim :
                curvelabel = fiber.name+" : "+"{:.2f}".format(fiber.lightloss()) 
                newcurve = CurveOnPlot(curvelabel, fiber.get_daily_average_data(), linestyle='-')
                endcapplot.add_curve(newcurve)
                newcurve = CurveOnPlot(curvelabel, fiber.linear_regression(finaledate = datefinale), linestyle=':')
                endcapplot.add_curve(newcurve)
                print("new curve in endcap : " + curvelabel)
                fibers_to_replace.append(fiber)
                if last_day_average < endcapminactual :
                    fiber.comment = "current lightpower = " + str(last_day_average) + " < 250"
                elif final_regression < endcapregressionlim :
                    fiber.comment = "according to final regression"

    limitecurvebarrel = CurveOnPlot('', [(datedebut, barrelregressionlim), (datefinale, barrelregressionlim)], linestyle='--', color='red')
    barrelplot.add_curve(limitecurvebarrel)
    limitecurveendcap = CurveOnPlot('', [(datedebut, endcapregressionlim), (datefinale, endcapregressionlim)], linestyle='--', color='red')
    endcapplot.add_curve(limitecurveendcap)

    # Close the database connection
    dbconnexion.close()
    
    # Set Y scale
    barrelplot.set_Yscale(0, 1000)
    endcapplot.set_Yscale(0, 1000)

    # Finnaly print the finale multiplot figure
    le_superplot = MultiPlot()
    le_superplot.add_plot(barrelplot)
    le_superplot.add_plot(endcapplot)

    print("superplot creation")
    le_superplot.make_plot( legend = False)

    # Get exchanged fiber list
    exchanged_fiber = get_ltdb_fibers_exchanged_list("ALL")
    exchanged_fiber_name = []
    for fiber in exchanged_fiber :
        exchanged_fiber_name.append(fiber.name)

    # Print fiber list to replace :
    print("***==========================================***")
    print("                 FIBERS TO REPLACE :            ")
    for fiber in fibers_to_replace :
        suffix = " : " + fiber.comment
        if fiber.name in exchanged_fiber_name :
            suffix = suffix + " /!\\but already changed fiber"
        print("" + fiber.name + suffix)
    print(" ***======================================***")

if __name__ == "__main__":
    main()
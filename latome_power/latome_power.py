from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle
import matplotlib.pyplot as plt
import numpy as np
import argparse
from array import array
import re
import mysql.connector

parent_dir = os.path.dirname(os.path.dirname(__file__))
lib_dir = os.path.join(parent_dir, 'lib')
sys.path.append(lib_dir)
from lib_plot import *
from lib_ldpb_component import *



def main():
    # Global variable for script :
    Y_plot_label = "Power consumtion "
    datedebut = '2024/10/01 00:00:00'
    datefinale = '2024/12/31 00:00:00'

    # LDPB list
    dt_setup = DT()
    ldpb_list = dt_setup.get_ldpb_list()

    # Connection to the database
    try:
        dbconnexion = cx_Oracle.connect("ATLAS_PVSS_READER", "PVSSRED4PRO", "ATONR_ADG")
    except cx_Oracle.DatabaseError as e:
        print(f"Database connection error: {e}")
        return

    # Parcourir La liste des LArC
    for ldpb in ldpb_list :

        # Create the two plot, barrel and endcap
        elplotlat1 = MultiCurve("LArC {} latome 1 power consumtion ".format(ldpb.name), Y_label = Y_plot_label, Ymin = 0, Ymax = 100)
        elplotlat2 = MultiCurve("LArC {} latome 2 power consumtion ".format(ldpb.name), Y_label = Y_plot_label, Ymin = 0, Ymax = 100)
        elplotlat3 = MultiCurve("LArC {} latome 3 power consumtion ".format(ldpb.name), Y_label = Y_plot_label, Ymin = 0, Ymax = 100)
        elplotlat4 = MultiCurve("LArC {} latome 4 power consumtion ".format(ldpb.name), Y_label = Y_plot_label, Ymin = 0, Ymax = 100)
        losplots = [elplotlat1, elplotlat2, elplotlat3, elplotlat4]

        colors = ['lightcoral', 'darkred', 'gold', 'orange', 'lightgreen', 'darkgreen', 'skyblue', 'navy']
        color_index = 0
        linestyles = ['-', '-', '-', '-']
        linestyle_index = 0

        for latome in ldpb.latomes:
            if latome != None :
                latome.get_points_P_amc(dbconnexion)
                latome.get_points_P_blade(dbconnexion)

                # Create curve labels with maximum power values
                curvelabel_amc = f'{ldpb.name} latome {latome.name} AMC power consumption'
                curvelabel_blade = f'{ldpb.name} latome {latome.name} Blade power consumption'

                power_consumption_amc_curve = CurveOnPlot(curvelabel_amc, latome.get_points_P_amc(dbconnexion), linestyle=linestyles[linestyle_index], color=colors[color_index])
                power_consumption_blade_curve = CurveOnPlot(curvelabel_blade, latome.get_points_P_blade(dbconnexion), linestyle=linestyles[linestyle_index], color=colors[color_index + 1])
                losplots[linestyle_index].add_curve(power_consumption_amc_curve)
                losplots[linestyle_index].add_curve(power_consumption_blade_curve)

                color_index = (color_index + 2) % len(colors)
                linestyle_index += 1

        # Create the plot
        le_superplot = MultiPlot()
        for plot in losplots:
            le_superplot.add_plot(plot)
        filename = "./plots/LDPB_{}_latomes_power_consumption".format(ldpb.name)
        le_superplot.make_plot( legend = True, save_path = filename)

    # Close the database connection
    dbconnexion.close()

if __name__ == "__main__":
    main()

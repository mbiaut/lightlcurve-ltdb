from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle
import matplotlib.pyplot as plt
import numpy as np
import argparse
import pickle
import matplotlib.dates as mdates
from array import array	
from scipy import stats


#class representing a curve
class CurveOnPlot:
    def __init__(self, curvename, listpoint=[], linestyle='-.', color='nodefined'):
        print(f"Inizializzazione della classe CurveOnPlot: {curvename}")
        self.x = []
        self.y = []
        self.curvename = curvename
        self.color = 'blue'
        self.colorfixed = False
        if color != 'nodefined':
            self.color = color
            self.colorfixed = True
        self.linestyle = linestyle
        if listpoint != []:
            self.assign_point(listpoint)

    def assign_point(self, tupleslist):
        print(f"Assegnazione dei punti alla curva: {self.curvename}")
        self.x = []
        self.y = []
        datasorted = []
        for untuple in tupleslist:
            datasorted.append(((datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()), untuple[1]))
        datasorted = sorted(datasorted, key=lambda x: x[0])
        for untuple in datasorted:
            self.x.append(untuple[0])
            self.y.append(untuple[1])
        self.x = [datetime.fromtimestamp(ts) for ts in self.x]


#class representing a multiplot object
class MultiPlot:
    def __init__(self):
        print("Inizializzazione della classe MultiPlot")
        self.liste_plot = []

    def add_plot(self, plot):
        print("Aggiunta di un plot alla lista")
        self.liste_plot.append(plot)

    def get_list_axe(self):
        listaxe = []
        for plot in self.liste_plot:
            listaxe.append(plot.get_plot)

        return listaxe
    
    def make_plot(self, legend=True, bar=False, save_path=None):
        print("Creazione del plot")
        n = len(self.liste_plot)
        if n == 4:
            rows, cols = 2, 2
        else:
            rows, cols = 1, n
        figure, axs = plt.subplots(rows, cols, figsize=(18, 9))
        axs = axs.flatten() if n > 1 else [axs]
        for i, plot in enumerate(self.liste_plot):
            plot.get_plot(axs[i], legend=legend, bar=bar)
        plt.tight_layout()
        if save_path:
            plt.savefig(save_path)
            print(f"Plot salvato in: {save_path}")
        else:
            plt.show()
        plt.close(figure)


#class representing a multicurve plot
class MultiCurve :
    def __init__(self, titre, Y_label, Ymin = 0, Ymax = 1000) :
        self.liste_curves = []
        self.title = titre
        self.Y_label = Y_label
        self.Ymin = Ymin
        self.Ymax = Ymax

    def colorise_curves(self) :
        arcenciel = plt.cm.viridis(np.linspace(0, 1, len(self.liste_curves)))
        for i in range(0, len(self.liste_curves)) :
            if not self.liste_curves[i].colorfixed :
                self.liste_curves[i].color = arcenciel[i]

    def add_curve(self, new_curve) :
        self.liste_curves.append(new_curve)
        self.colorise_curves()
    
    def set_Yscale(self, ymin, ymax) :
        self.Ymin = ymin
        self.Ymax = ymax

    def get_plot(self, ax, bar = False, legend = True):
        # Ajout de chaque courbe
        for curve in self.liste_curves:
            if bar is False :
                ax.plot(curve.x, curve.y, linestyle=curve.linestyle, label=curve.curvename, color=curve.color, linewidth=0.5)
            else :
                ax.bar(curve.x, curve.y, label=curve.curvename, color=curve.color)
        
        # Fixer les limites de l'axe y
        ax.set_ylim(self.Ymin, self.Ymax)

        # Set label on axes
        ax.set_xlabel('Date')
        ax.set_ylabel(self.Y_label)

        # Set legend
        if legend :
            ax.legend(loc='lower left')

        # Set plot titre
        ax.set_title(self.title)

        # Formater les dates
        ax.xaxis.set_major_locator(mdates.DayLocator(interval=10))  # Intervalle de 10 jours
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))  # Format de la date

        # Rotation des labels pour éviter qu'ils se chevauchent
        plt.gcf().autofmt_xdate()

        ax.set_xlabel('date')
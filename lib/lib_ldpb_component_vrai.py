from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle
import matplotlib.pyplot as plt
import numpy as np
import argparse
import pickle
import matplotlib.dates as mdates
from array import array	
from scipy import stats


# Function to get a list of tuples from db
def get_tuples_list_from_query(la_query, dbconnexion):
    print(f"Eseguendo query: {la_query}")
    current = dbconnexion.cursor()
    current.execute(la_query)
    result = current.fetchall()
    print(f"Numero di risultati ottenuti: {len(result)}")
    current.close()
    return result

def get_points_from_DCS(dbconnexion, dp):
    sqlCommand = "SELECT to_char(valeurs.ts,'yyyy/mm/dd hh24:mi:ss'),valeurs.value_number FROM ATLAS_PVSSLAR.eventhistory valeurs, ATLAS_PVSSLAR.elements datapoint WHERE datapoint.element_name='{0}' AND valeurs.element_id = datapoint.element_id".format(dp)
    sqlCommand += " AND valeurs.ts >= DATE '2024-10-01'"
    query = sqlCommand
    print(f"La query è: {query}")
    tuples_list = get_tuples_list_from_query(query, dbconnexion)
    return tuples_list

# Clase para representar todo el DT sisteme
class DT:
    def __init__(self):
        print("Inizializzazione della classe DT")
        self.atcas = {
            Atca("ATCA1", 1),
            Atca("ATCA2", 2),
            Atca("ATCA3", 3)
        }

    def get_ldpb_list(self):
        print("Ottenimento della lista di LDPB")
        ldpbs = []
        for atca in self.atcas:
            for ldpb in atca.ldpb:
                ldpbs.append(atca.ldpb[ldpb])
        print(f"Numero di LDPB ottenuti: {len(ldpbs)}")
        return ldpbs
    

# Clase para representar un ATCA
class Atca:
    def __init__(self, name, numero):
        print(f"Inizializzazione della classe Atca: {name}, numero: {numero}")
        self.name = name
        self.numero = numero
        self.ldpb = {}

        if numero == 1 :
            self.ldpb[3] = Ldpb(
                                parent = self,
                                name = "EMBC_4",
                                larc = Larc("Larc_EMBC_4"),
                                ipmc_num = 162, 
                                latomes = [
                                    Latome("LATOME_EMBC_13"),
                                    Latome("LATOME_EMBC_14"),
                                    Latome("LATOME_EMBC_15"),
                                    Latome("LATOME_EMBC_16")
                                ])
            self.ldpb[3].slot = 3

            self.ldpb[4] = Ldpb(
                                parent = self,
                                name = "EMBC_3",
                                larc = Larc("Larc_EMBC_3"),
                                ipmc_num = 181, 
                                latomes = [
                                    Latome("LATOME_EMBC_9"),
                                    Latome("LATOME_EMBC_10"),
                                    Latome("LATOME_EMBC_11"),
                                    Latome("LATOME_EMBC_12")
                                ])
            self.ldpb[4].slot = 4

            self.ldpb[5] = Ldpb(
                                parent = self,
                                name = "EMBC_2",
                                larc = Larc("Larc_EMBC_2"),
                                ipmc_num = 180, 
                                latomes = [
                                    Latome("LATOME_EMBC_5"),
                                    Latome("LATOME_EMBC_6"),
                                    Latome("LATOME_EMBC_7"),
                                    Latome("LATOME_EMBC_8")
                                ])
            self.ldpb[5].slot = 5

            self.ldpb[6] = Ldpb(
                                parent = self,
                                name = "EMBC_3",
                                larc = Larc("Larc_EMBC_3"),
                                ipmc_num = 183, 
                                latomes = [
                                    Latome("LATOME_EMBC_1"),
                                    Latome("LATOME_EMBC_2"),
                                    Latome("LATOME_EMBC_3"),
                                    Latome("LATOME_EMBC_4")
                                ])
            self.ldpb[6].slot = 6

            self.ldpb[9] = Ldpb(
                                parent = self,
                                name = "EMBC_EMECC_4",
                                larc = Larc("Larc_EMBC_EMECC_4"),
                                ipmc_num = 133, 
                                latomes = [
                                    Latome("LATOME_EMBC_EMECC_13"),
                                    Latome("LATOME_EMBC_EMECC_14"),
                                    Latome("LATOME_EMBC_EMECC_15"),
                                    Latome("LATOME_EMBC_EMECC_16")
                                ])
            self.ldpb[9].slot = 9

            self.ldpb[10] = Ldpb(
                                parent = self,
                                name = "EMBC_EMEC_3",
                                larc = Larc("Larc_EMBC_EMECC_3"),
                                ipmc_num = 193, 
                                latomes = [
                                    Latome("LATOME_EMBC_EMEC_9"),
                                    Latome("LATOME_EMBC_EMEC_10"),
                                    Latome("LATOME_EMBC_EMEC_11"),
                                    Latome("LATOME_EMBC_EMEC_12")
                                ])
            self.ldpb[10].slot = 10

            self.ldpb[11] = Ldpb(
                                parent = self,
                                name = "EMBC_EMECC_2",
                                larc = Larc("Larc_EMBC_EMECC_2"),
                                ipmc_num = 191, 
                                latomes = [
                                    Latome("LATOME_EMBC_EMECC_5"),
                                    Latome("LATOME_EMBC_EMECC_6"),
                                    Latome("LATOME_EMBC_EMECC_7"),
                                    Latome("LATOME_EMBC_EMECC_8")
                                ])
            self.ldpb[11].slot = 11

            self.ldpb[12] = Ldpb(
                                parent = self,
                                name = "EMBC_EMECC_1",
                                larc = Larc("Larc_EMBC_EMECC_1"),
                                ipmc_num = 155, 
                                latomes = [
                                    Latome("LATOME_EMBC_EMECC_1"),
                                    Latome("LATOME_EMBC_EMECC_2"),
                                    Latome("LATOME_EMBC_EMECC_3"),
                                    Latome("LATOME_EMBC_EMECC_4")
                                ])
            self.ldpb[12].slot = 12

            self.ldpb[13] = Ldpb(
                                parent = self,
                                name = "EMECC_HECC_2",
                                larc = Larc("Larc_EMECC_HECC_2"),
                                ipmc_num = 165, 
                                latomes = [
                                    Latome("LATOME_EMECC_HECC_5"),
                                    Latome("LATOME_EMECC_HECC_6"),
                                    Latome("LATOME_EMECC_HECC_7"),
                                    Latome("LATOME_EMECC_HECC_8")
                                ])
            self.ldpb[13].slot = 13

            self.ldpb[14] = Ldpb(
                                parent = self,
                                name = "EMECC_HECC_1",
                                larc = Larc("Larc_EMECC_HECC_1"),
                                ipmc_num = 161, 
                                latomes = [
                                    Latome("LATOME_EMECC_HECC_1"),
                                    Latome("LATOME_EMECC_HECC_2"),
                                    Latome("LATOME_EMECC_HECC_3"),
                                    Latome("LATOME_EMECC_HECC_4")
                                ])
            self.ldpb[14].slot = 14

        if numero == 2 :
            self.ldpb[3] = Ldpb(
                                parent = self,
                                name = "EMBA_4",
                                larc = Larc("Larc_EMBA_4"),
                                ipmc_num = 158, 
                                latomes = [
                                    Latome("LATOME_EMBA_13"),
                                    Latome("LATOME_EMBA_14"),
                                    Latome("LATOME_EMBA_15"),
                                    Latome("LATOME_EMBA_16")
                                ])
            self.ldpb[3].slot = 3

            self.ldpb[4] = Ldpb(
                                parent = self,
                                name = "EMBA_3",
                                larc = Larc("Larc_EMBA_3"),
                                ipmc_num = 174, 
                                latomes = [
                                    Latome("LATOME_EMBA_9"),
                                    Latome("LATOME_EMBA_10"),
                                    Latome("LATOME_EMBA_11"),
                                    Latome("LATOME_EMBA_12")
                                ])
            self.ldpb[4].slot = 4

            self.ldpb[5] = Ldpb(
                                parent = self,
                                name = "EMBA_2",
                                larc = Larc("Larc_EMBA_2"),
                                ipmc_num = 178, 
                                latomes = [
                                    Latome("LATOME_EMBA_5"),
                                    Latome("LATOME_EMBA_6"),
                                    Latome("LATOME_EMBA_7"),
                                    Latome("LATOME_EMBA_8")
                                ])
            self.ldpb[5].slot = 5

            self.ldpb[6] = Ldpb(
                                parent = self,
                                name = "EMBA_1",
                                larc = Larc("Larc_EMBA_1"),
                                ipmc_num = 218, 
                                latomes = [
                                    Latome("LATOME_EMBA_1"),
                                    Latome("LATOME_EMBA_2"),
                                    Latome("LATOME_EMBA_3"),
                                    Latome("LATOME_EMBA_4")
                                ])
            self.ldpb[6].slot = 6

            self.ldpb[9] = Ldpb(
                                parent = self,
                                name = "EMBA_EMECA_4",
                                larc = Larc("Larc_EMBA_EMECA_4"),
                                ipmc_num = 189, 
                                latomes = [
                                    Latome("LATOME_EMBA_EMECA_13"),
                                    Latome("LATOME_EMBA_EMECA_14"),
                                    Latome("LATOME_EMBA_EMECA_15"),
                                    Latome("LATOME_EMBA_EMECA_16")
                                ])
            self.ldpb[9].slot = 9

            self.ldpb[10] = Ldpb(
                                parent = self,
                                name = "EMBA_EMECA_3",
                                larc = Larc("Larc_EMBA_EMECA_3"),
                                ipmc_num = 188, 
                                latomes = [
                                    Latome("LATOME_EMBA_EMECA_9"),
                                    Latome("LATOME_EMBA_EMECA_10"),
                                    Latome("LATOME_EMBA_EMECA_11"),
                                    Latome("LATOME_EMBA_EMECA_12")
                                ])
            self.ldpb[10].slot = 10

            self.ldpb[11] = Ldpb(
                                parent = self,
                                name = "EMBA_EMECA_2",
                                larc = Larc("Larc_EMBA_EMECA_2"),
                                ipmc_num = 187, 
                                latomes = [
                                    Latome("LATOME_EMBA_EMECA_5"),
                                    Latome("LATOME_EMBA_EMECA_6"),
                                    Latome("LATOME_EMBA_EMECA_7"),
                                    Latome("LATOME_EMBA_EMECA_8")
                                ])
            self.ldpb[11].slot = 11

            self.ldpb[12] = Ldpb(
                                parent = self,
                                name = "EMBA_EMECA_1",
                                larc = Larc("Larc_EMBA_EMECA_1"),
                                ipmc_num = 186, 
                                latomes = [
                                    Latome("LATOME_EMBA_EMECA_1"),
                                    Latome("LATOME_EMBA_EMECA_2"),
                                    Latome("LATOME_EMBA_EMECA_3"),
                                    Latome("LATOME_EMBA_EMECA_4")
                                ])
            self.ldpb[12].slot = 12

            self.ldpb[13] = Ldpb(
                                parent = self,
                                name = "EMECA_HECA_2",
                                larc = Larc("Larc_EMECA_HECA_2"),
                                ipmc_num = 124, 
                                latomes = [
                                    Latome("LATOME_EMECA_HECA_5"),
                                    Latome("LATOME_EMECA_HECA_6"),
                                    Latome("LATOME_EMECA_HECA_7"),
                                    Latome("LATOME_EMECA_HECA_8")
                                ])
            self.ldpb[13].slot = 13

            self.ldpb[14] = Ldpb(
                                parent = self,
                                name = "EMECA_HECA_1",
                                larc = Larc("Larc_EMECA_HECA_1"),
                                ipmc_num = 159, 
                                latomes = [
                                    Latome("LATOME_EMECA_HECA_1"),
                                    Latome("LATOME_EMECA_HECA_2"),
                                    Latome("LATOME_EMECA_HECA_3"),
                                    Latome("LATOME_EMECA_HECA_4")
                                ])
            self.ldpb[14].slot = 14

        if numero == 3 :
            self.ldpb[2] = Ldpb(
                                parent = self,
                                name = "EMECA_4",
                                larc = Larc("Larc_EMECA_4"),
                                ipmc_num = 168, 
                                latomes = [
                                    Latome("LATOME_EMECA_13"),
                                    Latome("LATOME_EMECA_14"),
                                    Latome("LATOME_EMECA_15"),
                                    Latome("LATOME_EMECA_16")
                                ])
            self.ldpb[2].slot = 2

            self.ldpb[3] = Ldpb(
                                parent = self,
                                name = "EMECA_3",
                                larc = Larc("Larc_EMECA_3"),
                                ipmc_num = 173, 
                                latomes = [
                                    Latome("LATOME_EMECA_9"),
                                    Latome("LATOME_EMECA_10"),
                                    Latome("LATOME_EMECA_11"),
                                    Latome("LATOME_EMECA_12")
                                ])
            self.ldpb[3].slot = 3

            self.ldpb[4] = Ldpb(
                                parent = self,
                                name = "EMECA_2",
                                larc = Larc("Larc_EMECA_2"),
                                ipmc_num = 172, 
                                latomes = [
                                    Latome("LATOME_EMECA_5"),
                                    Latome("LATOME_EMECA_6"),
                                    Latome("LATOME_EMECA_7"),
                                    Latome("LATOME_EMECA_8")
                                ])
            self.ldpb[4].slot = 4

            self.ldpb[5] = Ldpb(
                                parent = self,
                                name = "EMECA_1",
                                larc = Larc("Larc_EMECA_1"),
                                ipmc_num = 154, 
                                latomes = [
                                    Latome("LATOME_EMECA_1"),
                                    Latome("LATOME_EMECA_2"),
                                    Latome("LATOME_EMECA_3"),
                                    Latome("LATOME_EMECA_4")
                                ])
            self.ldpb[5].slot = 5

            self.ldpb[6] = Ldpb(
                                parent = self,
                                name = "FCALA_1",
                                larc = Larc("Larc_FCALA_1"),
                                ipmc_num = 170, 
                                latomes = [
                                    Latome("LATOME_FCALA_1"),
                                    Latome("LATOME_FCALA_2"),
                                    Latome("None"),
                                    Latome("None")
                                ])
            self.ldpb[6].slot = 6

            self.ldpb[9] = Ldpb(
                                parent = self,
                                name = "FCALC_1",
                                larc = Larc("Larc_FCALC_1"),
                                ipmc_num = 185, 
                                latomes = [
                                    Latome("LATOME_FCALC_1"),
                                    Latome("LATOME_FCALC_2"),
                                    Latome("None"),
                                    Latome("None")
                                ])
            self.ldpb[9].slot = 9

            self.ldpb[10] = Ldpb(
                                parent = self,
                                name = "EMECC_4",
                                larc = Larc("Larc_EMECC_4"),
                                ipmc_num = 169, 
                                latomes = [
                                    Latome("LATOME_EMECC_13"),
                                    Latome("LATOME_EMECC_14"),
                                    Latome("LATOME_EMECC_15"),
                                    Latome("LATOME_EMECC_16")
                                ])
            self.ldpb[10].slot = 10

            self.ldpb[11] = Ldpb(
                                parent = self,
                                name = "EMECC_3",
                                larc = Larc("Larc_EMECC_3"),
                                ipmc_num = 179, 
                                latomes = [
                                    Latome("LATOME_EMECC_9"),
                                    Latome("LATOME_EMECC_10"),
                                    Latome("LATOME_EMECC_11"),
                                    Latome("LATOME_EMECC_12")
                                ])
            self.ldpb[11].slot = 11

            self.ldpb[12] = Ldpb(
                                parent = self,
                                name = "EMECC_2",
                                larc = Larc("Larc_EMECC_2"),
                                ipmc_num = 171, 
                                latomes = [
                                    Latome("LATOME_EMECC_5"),
                                    Latome("LATOME_EMECC_6"),
                                    Latome("LATOME_EMECC_7"),
                                    Latome("LATOME_EMECC_8")
                                ])
            self.ldpb[12].slot = 12

            self.ldpb[13] = Ldpb(
                                parent = self,
                                name = "EMECC_1",
                                larc = Larc("Larc_EMECC_1"),
                                ipmc_num = 167, 
                                latomes = [
                                    Latome("LATOME_EMECC_1"),
                                    Latome("LATOME_EMECC_2"),
                                    Latome("LATOME_EMECC_3"),
                                    Latome("LATOME_EMECC_4")
                                ])
            self.ldpb[13].slot = 13


#class representig a ltdb
class Ldpb:
    def __init__(self, parent, name, larc, ipmc_num, latomes=[None, None, None, None]):
        print(f"Inizializzazione della classe Ldpb: {name}")
        self.name = name
        self.larc = larc
        self.latomes = latomes
        self.atca = parent
        self.ipmc_num = ipmc_num
        self.ipmc_adress = None
        self._slot = None

        if self.latomes[0] != None :
            self.latomes[0].latome_number = 1
        if self.latomes[1] != None :
            self.latomes[1].latome_number = 2
        if self.latomes[2] != None :
            self.latomes[2].latome_number = 3
        if self.latomes[3] != None :
            self.latomes[3].latome_number = 4

        self.larc.ldpb = self
        self.latomes[0].ldpb = self
        self.latomes[1].ldpb = self
        self.latomes[2].ldpb = self
        self.latomes[3].ldpb = self

    def get_parent(self) :
        return self.atca
    
    @property
    def slot(self):
        return self._slot

    @slot.setter
    def slot(self, value):
        self._slot = value
        self.change_slot()

    def change_slot(self):
        if (self.slot == 2) : self.ipmc_adress = 150
        if (self.slot == 3) : self.ipmc_adress = 146
        if (self.slot == 4) : self.ipmc_adress = 142
        if (self.slot == 5) : self.ipmc_adress = 138
        if (self.slot == 6) : self.ipmc_adress = 134
        if (self.slot == 9) : self.ipmc_adress = 136
        if (self.slot == 10) : self.ipmc_adress = 140
        if (self.slot == 11) : self.ipmc_adress = 144
        if (self.slot == 12) : self.ipmc_adress = 148
        if (self.slot == 13) : self.ipmc_adress = 152
        if (self.slot == 14) : self.ipmc_adress = 156


# Clase para representado un larc
class Larc :
    def __init__(self, name) :
        self.name = name
        self.ldpb = None

    def get_parent(self) :
        return self.ldpb


# Classe per rappresentare un latome
class Latome:
    SENSOR_MAPPING = {
        1: (3, 4, 5, 6, 79, 74),
        2: (19, 20, 21, 22, 80, 75),
        3: (35, 36, 37, 38, 81, 76),
        4: (51, 52, 53, 54, 82, 77)
    }

    def __init__(self, name, latome_number=0):
        print(f"Inizializzazione della classe Latome: {name}, numero: {latome_number}")
        self.name = name
        self._latome_number = latome_number
        self.ldpb = None

        self.U_blade = []
        self.I_blade = []
        self.P_blade = []

        self.U_Vcc = []
        self.U_A10 = []
        self.I_Vcc = []
        self.I_A10 = []
        self.P_amc = []

        self.sensor_U_Vcc = None
        self.sensor_U_A10 = None
        self.sensor_I_Vcc = None
        self.sensor_I_A10 = None

        self.sensor_U_blade = None
        self.sensor_I_blade = None

        self.update_sensors()

    @property
    def latome_number(self):
        return self._latome_number

    @latome_number.setter
    def latome_number(self, value):
        self._latome_number = value
        self.update_sensors()

    def update_sensors(self):
        if self._latome_number in self.SENSOR_MAPPING:
            self.sensor_U_Vcc, self.sensor_U_A10, self.sensor_I_Vcc, self.sensor_I_A10, self.sensor_U_blade, self.sensor_I_blade = self.SENSOR_MAPPING[self._latome_number]
            print("Latome ", self.name, " has sensors:")
            print("    sensor U_Vcc: ", self.sensor_U_Vcc)
            print("    sensor U_A10: ", self.sensor_U_A10)
            print("    sensor I_Vcc: ", self.sensor_I_Vcc)
            print("    sensor I_A10: ", self.sensor_I_A10)
            print("    sensor U_blade: ", self.sensor_U_blade)
            print("    sensor I_blade: ", self.sensor_I_blade)

    def get_parent(self):
        return self.ldpb

    def get_points(self, sensor_type, dbconnexion, force_DCS=False):
        print(f"Ottenimento dei punti per {self.name}, sensore: {sensor_type}")
        filename = f"./savedpoints/{self.name}_{sensor_type}.pkl"
        file_exist = os.path.exists(filename)
        if file_exist and not force_DCS:
            print(f"Caricamento dei punti da file pickle: {filename}")
            self.load_points_pickle(sensor_type)
        else:
            print(f"Ottenimento dei punti da DCS per {self.name}, sensore: {sensor_type}")
            self.get_sensor_points_from_DCS(sensor_type, dbconnexion)
            self.save_points_pickle(sensor_type)

    def load_points_pickle(self, sensor_type):
        filename = f"./savedpoints/{self.name}_{sensor_type}.pkl"
        with open(filename, 'rb') as savfile:
            # Carica i dati dal file pickle
            pass

    def save_points_pickle(self, sensor_type):
        filename = f"./savedpoints/{self.name}_{sensor_type}.pkl"
        with open(filename, 'wb') as savfile:
            # Salva i dati nel file pickle
            pass

    def get_sensor_points_from_DCS(self, sensor_type, dbconnexion):
        print(f"get points {sensor_type} from DCS : " + self.name)
        dro_number = f"{self.ldpb.atca.numero:02d}"
        dp = f'ATLLARDTATCA:ASM-LAR-DRO-{dro_number}/IPMC{self.ldpb.ipmc_adress}/Sensor{getattr(self, f"sensor_{sensor_type}")}.reading'
        setattr(self, sensor_type, get_points_from_DCS(dbconnexion, dp))
        # mostrare il resultato
        print("Punti ottenuti: "+str(getattr(self, sensor_type)))

    # Funzioni esistenti modificate per utilizzare la funzione generica
    def get_points_U_Vcc(self, dbconnexion, force_DCS=False):
        self.get_points("U_Vcc", dbconnexion, force_DCS)

    def get_points_U_A10(self, dbconnexion, force_DCS=False):
        self.get_points("U_A10", dbconnexion, force_DCS)

    def get_points_I_Vcc(self, dbconnexion, force_DCS=False):
        self.get_points("I_Vcc", dbconnexion, force_DCS)

    def get_points_I_A10(self, dbconnexion, force_DCS=False):
        self.get_points("I_A10", dbconnexion, force_DCS)

    def get_points_U_blade(self, dbconnexion, force_DCS=False):
        self.get_points("U_blade", dbconnexion, force_DCS)

    def get_points_I_blade(self, dbconnexion, force_DCS=False):
        self.get_points("I_blade", dbconnexion, force_DCS)


    def calculate_power_consumption_amc(self):
        print(f"Calcolo del consumo di potenza AMC per {self.name}")
        self.P_amc = []
        for u_vcc in self.U_Vcc:
            closest_i_vcc = min(self.I_Vcc, key=lambda x: abs(datetime.strptime(x[0], '%Y/%m/%d %H:%M:%S') - datetime.strptime(u_vcc[0], '%Y/%m/%d %H:%M:%S')))
            closest_u_a10 = min(self.U_A10, key=lambda x: abs(datetime.strptime(x[0], '%Y/%m/%d %H:%M:%S') - datetime.strptime(u_vcc[0], '%Y/%m/%d %H:%M:%S')))
            closest_i_a10 = min(self.I_A10, key=lambda x: abs(datetime.strptime(x[0], '%Y/%m/%d %H:%M:%S') - datetime.strptime(u_vcc[0], '%Y/%m/%d %H:%M:%S')))
            power = u_vcc[1] * closest_i_vcc[1] + closest_u_a10[1] * closest_i_a10[1]
            self.P_amc.append((u_vcc[0], power))

    def calculate_power_consumption_blade(self):
        print(f"Calcolo del consumo di potenza Blade per {self.name}")
        self.P_blade = []
        for u_blade in self.U_blade:
            closest_i_blade = min(self.I_blade, key=lambda x: abs(datetime.strptime(x[0], '%Y/%m/%d %H:%M:%S') - datetime.strptime(u_blade[0], '%Y/%m/%d %H:%M:%S')))
            power = u_blade[1] * closest_i_blade[1]
            self.P_blade.append((u_blade[0], power))

    def get_points_P_amc(self, dbconnexion, force_DCS=False) :
        self.get_points_U_Vcc(dbconnexion, force_DCS)
        self.get_points_U_A10(dbconnexion, force_DCS)
        self.get_points_I_Vcc(dbconnexion, force_DCS)
        self.get_points_I_A10(dbconnexion, force_DCS)
        self.calculate_power_consumption_amc()
        return self.P_amc
    
    def get_points_P_blade(self, dbconnexion, force_DCS=False) :
        self.get_points_U_blade(dbconnexion, force_DCS)
        self.get_points_I_blade(dbconnexion, force_DCS)
        self.calculate_power_consumption_blade()
        return self.P_blade

    def save_calculated_points_pickle(self, points, filename):
        with open(filename, 'wb') as savfile:
            pickle.dump(points, savfile)

    def load_calculated_points_pickle(self, filename):
        with open(filename, 'rb') as savfile:
            return pickle.load(savfile)

    def calculate_power_consumption_blade(self, force_recalculate=False):
        filename = f"./savedcalculatedpoints/{self.name}_P_blade.pkl"
        if os.path.exists(filename) and not force_recalculate:
            print(f"Caricamento dei punti calcolati da file pickle: {filename}")
            self.P_blade = self.load_calculated_points_pickle(filename)
        else:
            print(f"Calcolo del consumo di potenza Blade per {self.name}")
            self.P_blade = []
            for u_blade in self.U_blade:
                closest_i_blade = min(self.I_blade, key=lambda x: abs(datetime.strptime(x[0], '%Y/%m/%d %H:%M:%S') - datetime.strptime(u_blade[0], '%Y/%m/%d %H:%M:%S')))
                power = u_blade[1] * closest_i_blade[1]
                self.P_blade.append((u_blade[0], power))
            self.save_calculated_points_pickle(self.P_blade, filename)

    def calculate_power_consumption_amc(self, force_recalculate=False):
        filename = f"./savedcalculatedpoints/{self.name}_P_amc.pkl"
        if os.path.exists(filename) and not force_recalculate:
            print(f"Caricamento dei punti calcolati da file pickle: {filename}")
            self.P_amc = self.load_calculated_points_pickle(filename)
        else:
            print(f"Calcolo del consumo di potenza AMC per {self.name}")
            self.P_amc = []
            for u_vcc in self.U_Vcc:
                closest_i_vcc = min(self.I_Vcc, key=lambda x: abs(datetime.strptime(x[0], '%Y/%m/%d %H:%M:%S') - datetime.strptime(u_vcc[0], '%Y/%m/%d %H:%M:%S')))
                closest_u_a10 = min(self.U_A10, key=lambda x: abs(datetime.strptime(x[0], '%Y/%m/%d %H:%M:%S') - datetime.strptime(u_vcc[0], '%Y/%m/%d %H:%M:%S')))
                closest_i_a10 = min(self.I_A10, key=lambda x: abs(datetime.strptime(x[0], '%Y/%m/%d %H:%M:%S') - datetime.strptime(u_vcc[0], '%Y/%m/%d %H:%M:%S')))
                power = u_vcc[1] * closest_i_vcc[1] + closest_u_a10[1] * closest_i_a10[1]
                self.P_amc.append((u_vcc[0], power))
            self.save_calculated_points_pickle(self.P_amc, filename)

#class representing a ltdb fiber
class LtdbFiber :
    def __init__(self, ltdb, number) :
        self.ltdb = self.adaptltdbname(ltdb)
        self.number = number
        self.name = ltdb + "_fiber_" + str(number)
        self.raw_data = []
        self.cooked_data = []
        self.comment = ""

    def adaptltdbname(self, name) :
        name = name.replace("Spe0", "L")
        name = name.replace("Spe1", "R")
        name = name.replace("F1", "L")
        name = name.replace("F0", "R")
        name = name.replace("HEC", "H")
        return name

    def get_point(self, dbconnexion, force_DCS = False) :
        filename = "./savedpoints/" + self.ltdb + "_fiber_" + str(self.number) + ".pkl"
        file_exist = os.path.exists(filename)
        
        if file_exist and not force_DCS :
            self.get_point_pickle()

        else :
            self.get_point_from_DCS(dbconnexion)
            self.save_point_pickle()

        if self.raw_data == [] :
            print("WARNING : " + self.ltdb + " fiber " + str(self.number) + " doesn't have any point, will not be printed")

        self.cooked_data = self.raw_data 

    def save_point_pickle(self) :
        filename = "./savedpoints/" + self.ltdb + "_fiber_" + str(self.number) + ".pkl"
        # Ouvrir un fichier en mode binaire pour écrire
        with open(filename, 'wb') as savfile:
            # Sérialiser l'objet et l'écrire dans le fichier
            pickle.dump(self.raw_data, savfile)

    def get_point_pickle(self) :
        print("get point from pickle : " + self.ltdb + " " + str(self.number))
        filename = "./savedpoints/" + self.ltdb + "_fiber_" + str(self.number) + ".pkl"
        # Ouvrir le fichier en mode binaire pour lire
        with open(filename, 'rb') as savfile:
            # Lire le contenu du fichier et désérialiser l'objet
            self.raw_data = pickle.load(savfile)

    def get_point_from_DCS(self, dbconnexion) :
        print("get point from DCS : " + self.ltdb + " " + str(self.number))
        print("regex = " + "(.*)" + self.ltdb + "_" + str(self.number) + "(?!\d)(.*)")
        laregex = "_{}_{}\s".format(self.ltdb, str(self.number))
        query = "SELECT to_char(valeurs.ts,'yyyy/mm/dd hh24:mi:ss'), valeurs.value_number FROM ATLAS_PVSSLAR.eventhistory valeurs, ATLAS_PVSSLAR.elements datapoint WHERE valeurs.element_id = datapoint.element_id AND regexp_like(datapoint.COMMENT_, '" + laregex + "') ORDER BY valeurs.ts ASC";
        # query = "SELECT to_char(valeurs.ts,'yyyy/mm/dd hh24:mi:ss'), valeurs.value_number FROM ATLAS_PVSSLAR.eventhistory valeurs, ATLAS_PVSSLAR.elements datapoint WHERE valeurs.element_id = datapoint.element_id AND regexp_like(datapoint.COMMENT_, '') ORDER BY valeurs.ts ASC";
        print("interrogation : "+self.ltdb + " fiber " + str(self.number))
        self.raw_data = get_tuples_list_from_query(query, dbconnexion)
        print(len(self.raw_data))

    def cut_at_first_march24(self) :
        current_data = self.cooked_data
        self.cooked_data = []

        datedebut = datetime.strptime('2024/03/01 00:00:00', '%Y/%m/%d %H:%M:%S').timestamp()
        for untuple in current_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            if datedebut < date :
                self.cooked_data.append(untuple)
        
        return self.cooked_data

    def cut_at_20_april24(self) :
        current_data = self.cooked_data
        self.cooked_data = []

        datedebut = datetime.strptime('2024/04/20 00:00:00', '%Y/%m/%d %H:%M:%S').timestamp()
        for untuple in current_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            if datedebut < date :
                self.cooked_data.append(untuple)
        
        return self.cooked_data

    def cut_at_date(self, datedebutstr) :
        current_data = self.cooked_data
        self.cooked_data = []

        datedebut = datetime.strptime(datedebutstr, '%Y/%m/%d %H:%M:%S').timestamp()
        for untuple in current_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            if datedebut < date :
                self.cooked_data.append(untuple)
        
        return self.cooked_data

    def filter_data(self) :
        filtered_result = []
        for date, value in self.cooked_data :
            if 0 < value and value <= 3000 :
                filtered_result.append((date, value))
        self.cooked_data = filtered_result

        return self.cooked_data

    def get_relativepertintegral_data(self) :
        relativepert_points = []

        lightat20febr24 = self.cooked_data[0][1];
        integrallight = 0;
        i = 0
        for untuple in self.cooked_data :     
            if 0 < i :
                integrallight = integrallight + (untuple[1]-lightat20febr24)
                currentmoyenne = integrallight/i
                currentrelativepert = currentmoyenne/lightat20febr24*100
                relativepert_points.append((untuple[0], currentrelativepert))
            else :
                relativepert_points.append((untuple[0], 0))
            i = i + 1
        
        return relativepert_points

    def get_relativepert_data(self) :
        relativepert_points = []

        lightat20febr24 = self.cooked_data[0][1];
        integrallight = 0;
        for untuple in self.user_asked_data :
            if lightat20febr24 == 0 :
                lightat20febr24 = untuple[1]
            currentpert = (untuple[1]-lightat20febr24)
            currentrelativepert = currentpert/lightat20febr24*100
            relativepert_points.append((untuple[0], currentrelativepert))
        
        return relativepert_points

    
    def get_relativepert_data(self) :
        relativepert_points = []

        lightat20febr24 = self.cooked_data[0][1];
        integrallight = 0;
        for untuple in self.cooked_data :
            currentpert = (untuple[1]-lightat20febr24)
            currentrelativepert = currentpert/lightat20febr24*100
            relativepert_points.append((untuple[0], currentrelativepert))
        
        return relativepert_points

    def get_relativepert_dailymoyenned_data(self) :
        relativepert_dailymoyenned_points = []

        firstlight = self.cooked_data[0][1]
        firstindice = 0
        firstimestamp = datetime.strptime(self.cooked_data[0][0], '%Y/%m/%d %H:%M:%S').timestamp()
        currentimestamp = datetime.strptime(self.cooked_data[0][0], '%Y/%m/%d %H:%M:%S').timestamp()
                    
        currentmoyenne = 0
        j = 0
        # Moyenne by day
        for untuple in self.cooked_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            currentmoyenne += untuple[1] - firstlight
            j += 1
            if (currentimestamp + 3600*24) < date :
                moyenne = currentmoyenne/j/firstlight*100
                currentmoyenne = 0
                j = 0
                currentimestamp = date
                relativepert_dailymoyenned_points.append((untuple[0], moyenne))
        
        return relativepert_dailymoyenned_points

    def get_dailymoyenned_data(self) :
        relativepert_dailymoyenned_points = []

        firstlight = self.cooked_data[0][1]
        firstindice = 0
        firstimestamp = datetime.strptime(self.cooked_data[0][0], '%Y/%m/%d %H:%M:%S').timestamp()
        currentimestamp = datetime.strptime(self.cooked_data[0][0], '%Y/%m/%d %H:%M:%S').timestamp()
                    
        currentmoyenne = 0
        j = 0
        # Moyenne by day
        for untuple in self.cooked_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            currentmoyenne += untuple[1]
            j += 1
            if (currentimestamp + 3600*24) < date :
                moyenne = currentmoyenne/j
                currentmoyenne = 0
                j = 0
                currentimestamp = date
                relativepert_dailymoyenned_points.append((untuple[0], moyenne))
        
        return relativepert_dailymoyenned_points

    def get_daily_dico(self) :
        daily_dico = {}
        for untuple in self.cooked_data :
            theday = untuple[0].split(' ')[0]
            if theday not in daily_dico :
                daily_dico[theday] = []
            daily_dico[theday].append(untuple[1])

        return daily_dico

    def get_daily_average_data(self) :
        daily_dico = self.get_daily_dico()
        daily_average_data = []

        for day in daily_dico :
            daily_average = 0
            for val in daily_dico[day] :
                daily_average += val
            daily_average = daily_average / len(daily_dico[day])
            date  = day + ' 00:00:00'
            daily_average_data.append((date, daily_average))

        return daily_average_data

    def get_last_day_average(self) :
        daily_average_data = self.get_daily_average_data()
        return daily_average_data[len(daily_average_data)-1][1]

    def get_daily_lightloss_data(self) :
        firstlight = self.cooked_data[0][1]
        daily_average_data = self.get_daily_average_data()
        daily_lightloss_data = []
        for untuple in daily_average_data :
            currentlightloss = (untuple[1]-firstlight)/firstlight*100
            daily_lightloss_data.append((untuple[0], currentlightloss))

        return daily_lightloss_data

    def lightloss_integral(self) :
        datedebut = datetime.strptime('2024/03/01 00:00:00', '%Y/%m/%d %H:%M:%S').timestamp()
        lightat20febr24 = 0;
        integrallight = 0;
        i = 0
        for untuple in self.cooked_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            if datedebut < date :
                if lightat20febr24 == 0 :
                    lightat20febr24 = untuple[1]
                integrallight = integrallight + (untuple[1]-lightat20febr24)
                i += 1

        moyenne = integrallight/i
        relativepert = moyenne/lightat20febr24*100

        return relativepert
  
    def lightloss(self) :
        relativepert_dailymoyenned_points = []

        datedebut = datetime.strptime('2024/03/01 00:00:00', '%Y/%m/%d %H:%M:%S').timestamp()
        firstlight = 0;
        firstindice = 0;
        firstimestamp = 0;
        currentimestamp = 0;

        # Find first point
        i = 0
        while firstlight == 0 :
            date = datetime.strptime(self.cooked_data[i][0], '%Y/%m/%d %H:%M:%S').timestamp()
            if datedebut < date :
                firstlight = self.cooked_data[i][1];
                firstindice = i
                firstimestamp = date
                currentimestamp = date
            i += 1
                    
        currentmoyenne = 0
        j = 0
        # Moyenne by day
        for untuple in self.cooked_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            if firstimestamp < date :
                currentmoyenne += untuple[1] - firstlight
                j += 1
                if (currentimestamp + 3600*24) < date :
                    moyenne = currentmoyenne/j/firstlight*100
                    currentmoyenne = 0
                    j = 0
                    currentimestamp = date
                    relativepert_dailymoyenned_points.append((untuple[0], moyenne))
        
        return relativepert_dailymoyenned_points[len(relativepert_dailymoyenned_points)-1][1]
            
    def notempty(self) :
        if self.raw_data == [] :
            return False
        else :
            return True

    def linear_regression(self, finaledate = "notspecified") :
        if finaledate == "notspecified" :
            finaledate = self.cooked_data[len(self.cooked_data)-1][0]

        x = []
        y = []
        datasorted = []

        for untuple in self.cooked_data :
            datasorted.append(((datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()), untuple[1]))
        datasorted = sorted(datasorted, key=lambda x: x[0])
        for untuple in datasorted :
            x.append(untuple[0])
            y.append(untuple[1])

        a, b, r_value, p_value, std_err = stats.linregress(x, y)

        droite = []
        x1 = datetime.strptime(self.cooked_data[0][0], '%Y/%m/%d %H:%M:%S').timestamp()
        x2 = datetime.strptime(finaledate, '%Y/%m/%d %H:%M:%S').timestamp()
        y1 = a*x1 + b
        y2 = a*x2 + b
        droite = [
            (self.cooked_data[0][0], y1), 
            (finaledate, y2)
            ]

        return droite

    def get_last_point_from_DCS(self, dbconnexion) :
        print("get point from DCS : " + self.ltdb + " " + str(self.number))
        query = "SELECT to_char(valeurs.ts,'yyyy/mm/dd hh24:mi:ss'),valeurs.value_number FROM ATLAS_PVSSLAR.eventhistory valeurs, ATLAS_PVSSLAR.elements datapoint WHERE valeurs.element_id = datapoint.element_id AND regexp_like(datapoint.COMMENT_,'(.*)" + self.ltdb + "_\\b" + str(self.number) + "\\b(.*)') ORDER BY valeurs.ts ASC";
        print("interrogation : "+self.ltdb + " fiber " + str(self.number))
        self.raw_data = get_tuples_list_from_query(query, dbconnexion)
from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle

import ROOT
from array import array


dbconnexion = cx_Oracle.connect("ATLAS_PVSS_READER","PVSSRED4PRO","ATONR_ADG")

def get_tuple_from_query(la_query):
    current = dbconnexion.cursor()
    current.execute(la_query)
    result = current.fetchall()
    #desc = [row[0] for row in cur.description]
    current.close()
    return result


def get_tuple_for_an_ltdb_fiber(ltdb, fibernum) :
    #query = "SELECT datapoint.element_name FROM ATLAS_PVSSLAR.elements datapoint WHERE regexp_like(datapoint.COMMENT_,'(.*)" + ltdb + " fiber " + str(fibernum) + "(.*)') ";
    #query = "SELECT datapoint.element_name FROM ATLAS_PVSSLAR.elements datapoint WHERE regexp_like(datapoint.COMMENT_,'(.*)I02R fiber 39(.*)') ";
    query = "SELECT to_char(valeurs.ts,'yyyy/mm/dd hh24:mi:ss'),valeurs.value_number FROM ATLAS_PVSSLAR.eventhistory valeurs, ATLAS_PVSSLAR.elements datapoint WHERE valeurs.element_id = datapoint.element_id AND regexp_like(datapoint.COMMENT_,'(.*)" + ltdb + " fiber " + str(fibernum) + "(.*)') ORDER BY valeurs.ts ASC";
    result = get_tuple_from_query(query)
    #filtered bad value too hight
    filtered_result = [(date, value) for date, value in result if value <= 3000]
    return filtered_result


def print_a_graph_from_a_tuple_list(data) :
    # Convertir les dates en secondes depuis l'époque UNIX
    times = [datetime.strptime(d[0], '%Y/%m/%d %H:%M:%S').timestamp() for d in data]
    values = [d[1] for d in data]

    # Convertir les listes en tableaux pour ROOT
    x = array('d', times)
    y = array('d', values)

    # Créer un TGraph
    graph = ROOT.TGraph(len(x), x, y)

    # Style du graphe
    graph.SetTitle("Mesures au fil du temps;Date;Valeur")
    graph.SetMarkerStyle(20)

    # Configurer l'axe des X pour afficher des dates lisibles
    graph.GetXaxis().SetTimeDisplay(1)
    graph.GetXaxis().SetTimeFormat("#splitline{%d/%m/%y}{%H:%M:%S}")
    graph.GetXaxis().SetTimeOffset(0, "gmt")  # Assurer que l'offset est correct
    graph.GetXaxis().SetNdivisions(-503)  # Optional: Customize the number of divisions/ticks

    # Créer un canvas
    c1 = ROOT.TCanvas("c1", "Graphique des mesures", 800, 600)

    # Dessiner le graphe
    graph.Draw("AP")

    # Afficher le canvas
    c1.Update()
    c1.Draw()

    # Sauvegarder le graphique en fichier image
    c1.SaveAs("graph.png")

ltdb = "I02R"
fibernum = 39
#data = get_tuple_for_an_ltdb_fiber(ltdb, fibernum)
#print(data)

data = [('2023/04/24 13:13:24', 707.0), ('2023/04/24 15:00:48', 703.0), 
        ('2023/04/24 16:48:21', 703.0), ('2023/04/24 18:24:57', 703.0), 
        ('2023/04/24 20:01:54', 703.0), ('2023/04/24 21:38:49', 703.0), 
        ('2023/04/24 23:15:52', 703.0), ('2023/04/25 00:52:51', 703.0), 
        ('2023/04/25 02:29:54', 703.0), ('2023/04/25 04:00:00', 701.0)]

print_a_graph_from_a_tuple_list(data)

#query = "SELECT datapoint.element_name FROM ATLAS_PVSSLAR.elements datapoint WHERE regexp_like(datapoint.COMMENT_,'(.*)I02R fiber 39(.*)') ";
#print(query)
#data = get_tuple_from_query(query)
#print(data)

#query = "SELECT to_char(valeurs.ts,'yyyy/mm/dd hh24:mi:ss'),valeurs.value_number FROM ATLAS_PVSSLAR.eventhistory valeurs, ATLAS_PVSSLAR.elements datapoint WHERE valeurs.element_id = datapoint.element_id AND regexp_like(datapoint.COMMENT_,'(.*)I02R fiber 39(.*)') ORDER BY valeurs.ts ASC";
#print(query)
#data = get_tuple_from_query(query)
#print(data)

#query = "SELECT to_char(valeurs.ts,'yyyy/mm/dd hh24:mi:ss'),valeurs.value_number FROM ATLAS_PVSSLAR.eventhistory valeurs, ATLAS_PVSSLAR.elements datapoint WHERE datapoint.element_name='ATLLARLDPB:LArC_EMBC_EMECC_1.latome2.upods.upod5.light2' AND valeurs.element_id = datapoint.element_id";
#print(query)
#data = get_tuple_from_query(query)
#print(data)
from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle
import matplotlib.pyplot as plt
import numpy as np
import argparse


import ROOT
from array import array


def get_tuple_from_query(la_query):
    current = dbconnexion.cursor()
    current.execute(la_query)
    result = current.fetchall()
    #desc = [row[0] for row in cur.description]
    current.close()
    return result


class LtdbFiber :
    def __init__(ltdb, number) :
        self.ltdb = ltdb
        self.fiber = fiber


def get_ltdb_fibers_list() :
    liste_to_return = []
    liste_to_return.append(LtdbFiber("I02R", 40))
    liste_to_return.append(LtdbFiber("I02R", 39))
    return


def get_tuple_for_an_ltdb_fiber(ltdb, fibernum) :
    query = "SELECT to_char(valeurs.ts,'yyyy/mm/dd hh24:mi:ss'),valeurs.value_number FROM ATLAS_PVSSLAR.eventhistory valeurs, ATLAS_PVSSLAR.elements datapoint WHERE valeurs.element_id = datapoint.element_id AND regexp_like(datapoint.COMMENT_,'(.*)" + ltdb + " fiber " + str(fibernum) + "(.*)') ORDER BY valeurs.ts ASC";
    result = get_tuple_from_query(query)
    return result


def filter_ltdb_light(data) :
    filtered_result = []
    for date, value in data :
        if 0 < value and value <= 3000 :
            filtered_result.append((date, value))
    return filtered_result


def print_a_root_graph_from_a_tuple_list_chatgpt(data) :
    # Convertir les dates en secondes depuis l'époque UNIX
    times = [datetime.strptime(d[0], '%Y/%m/%d %H:%M:%S').timestamp() for d in data]
    values = [d[1] for d in data]

    # Convertir les listes en tableaux pour ROOT
    x = array('d', times)
    y = array('d', values)

    # Créer un TGraph
    graph = ROOT.TGraph(len(x), x, y)

    # Style du graphe
    graph.SetTitle("Mesures au fil du temps;Date;Valeur")
    graph.SetMarkerStyle(20)

    # Configurer l'axe des X pour afficher des dates lisibles
    graph.GetXaxis().SetTimeDisplay(1)
    graph.GetXaxis().SetTimeFormat("#splitline{%d/%m/%y}{%H:%M:%S}")
    graph.GetXaxis().SetTimeOffset(0, "gmt")
    graph.GetXaxis().SetNdivisions(-503)

    # Créer un canvas
    c1 = ROOT.TCanvas("c1", "Graphique des mesures", 800, 600)

    # Dessiner le graphe
    graph.Draw("AP")

    # Afficher le canvas
    c1.Update()
    c1.Draw()

    # Sauvegarder le graphique en fichier image
    c1.SaveAs("root_graph.png")


def print_a_root_graph_from_a_tuple_list(data) :
    # Convertir les dates en secondes depuis l'époque UNIX
    times = []
    values = []
    for untuple in data :
        times.append(datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp())
        values.append(untuple[1])

    # Convertir les listes en tableaux pour ROOT
    x = array('d', times)
    y = array('d', values)

    # Créer un TGraph
    legraphe = ROOT.TGraph(len(x), x, y)

    # Style du graphe
    legraphe.SetTitle("Mesures au fil du temps;Date;Valeur")
    legraphe.SetMarkerStyle(20)

    # Configurer l'axe des X pour afficher des dates lisibles
    legraphe.GetXaxis().SetTimeDisplay(1)
    legraphe.GetXaxis().SetTimeFormat("#splitline{%d/%m/%y}{%H:%M:%S}")
    legraphe.GetXaxis().SetTimeOffset(0, "gmt")
    legraphe.GetXaxis().SetNdivisions(-503)

    # Créer un canvas
    lecanvas = ROOT.TCanvas("c1", "Graphique des mesures", 800, 600)

    # Dessiner le graphe
    legraphe.Draw("AP")

    # Afficher le canvas
    lecanvas.Update()
    lecanvas.Draw()

    # Sauvegarder le graphique en fichier image
    lecanvas.SaveAs("root_graph.png")

def print_a_matplotlib_graph_from_a_tuple_list(data, curvename, color) :
    # Convertir les dates en secondes depuis l'époque UNIX et mise en place dans les tableau x et y
    x = []
    y = []
    for untuple in data :
        x.append(datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp())
        y.append(untuple[1])

    # Creer la figure matplotlib en specifiant sa taille
    plt.figure(figsize=(10, 6))
    plt.plot(x, y, linestyle='-', label=curvename, color=color)

    # Ajouter des étiquettes et un titre
    plt.xlabel('date')
    plt.ylabel('lightpower')
    plt.title("Evolution de la lumiere")

    # Afficher la légende
    plt.legend()

    # Formater les dates sur l'axe des X
    plt.gcf().autofmt_xdate()  # Rotation automatique des dates pour une meilleure lisibilité

    # Enregistrer le graphique en tant qu'image PNG
    plt.savefig("./matplotlib_graph.png")

    # Fermer le graphique pour passer au suivant
    plt.close()




dbconnexion = cx_Oracle.connect("ATLAS_PVSS_READER","PVSSRED4PRO","ATONR_ADG")

ltdb = "I02R"
fibernum = 39
data = get_tuple_for_an_ltdb_fiber(ltdb, fibernum)
filtered_data = filter_ltdb_light(data)
print(filtered_data)
#print_a_root_graph_from_a_tuple_list(filtered_data)
print_a_matplotlib_graph_from_a_tuple_list(filtered_data)


def main():
    # Argument parsing
    parser = argparse.ArgumentParser(description='Generate LTDB light power graph from the DCS database')
    #parser.add_argument('--ltdb', type=str, required=True, help='LTDB name')
    #parser.add_argument('--fibernum', type=int, required=True, help='Fiber number')
    #parser.add_argument('--threshold', type=float, default=3000, help='Threshold to filter data')
    args = parser.parse_args()

    # Connection to the database
    try:
        dbconnexion = cx_Oracle.connect("ATLAS_PVSS_READER", "PVSSRED4PRO", "ATONR_ADG")
    except cx_Oracle.DatabaseError as e:
        print(f"Database connection error: {e}")
        return

    # Get list of fiber to watch
    ltdb_fibers_list = get_ltdb_fibers_list()

    # Get list of curve
    list_curves = []
    for fiber in ltdb_fibers_list :
        curvename = fiber.ltdb + " " + str(fiber.number)
        new_list = get_tuple_for_an_ltdb_fiber(fiber.ltdb, fiber.number)
        new_list_filtered = filter_ltdb_light(new_list)
        list_curves.append((new_list_filtered, curvename))

    
    

    # Close the database connection
    dbconnexion.close()

if __name__ == "__main__":
    main()

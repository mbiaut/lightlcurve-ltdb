from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle
import matplotlib.pyplot as plt
import numpy as np
import argparse
import pickle
import matplotlib.dates as mdates
from array import array	
from scipy import stats


#class representing a curve
class CurveOnPlot :
    def __init__(self, curvename, listpoint = [], linestyle='-.', color='nodefined') :
        self.x = []
        self.y = []
        self.curvename = curvename
        self.color = 'blue'
        self.colorfixed = False
        if color != 'nodefined' :
            self.color = color
            self.colorfixed = True
        self.linestyle = linestyle
        if listpoint != [] :
            self.assign_point(listpoint)

    def assign_point(self, tupleslist) :
        self.x = []
        self.y = []

        datasorted = []

        for untuple in tupleslist :
            datasorted.append(((datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()), untuple[1]))

        datasorted = sorted(datasorted, key=lambda x: x[0])

        for untuple in datasorted :
            self.x.append(untuple[0])
            self.y.append(untuple[1])

        self.x = [datetime.fromtimestamp(ts) for ts in self.x]


#class representing a multiplot object
class MultiPlot :
    def __init__(self) :
        self.liste_plot = []

    def add_plot(self, plot) :
        self.liste_plot.append(plot)

    def get_list_axe(self) :
        listaxe = []
        for plot in self.liste_plot :
            listaxe.append(plot.get_plot)

        return listaxe
    
    def make_plot(self, legend = True):
        # Calcul des dimensions de la grille (n lignes et 1 colonne ou plus, selon la préférence)
        n = len(self.liste_plot)
        rows = 1
        cols = n

        # Création de la figure et des sous-graphiques
        figure, axs = plt.subplots(rows, cols, figsize=(18, 9))
        if n == 1:
            axs = [axs]  # Pour uniformiser l'accès lorsque n = 1

        # Ajout de chaque plot aux sous-graphiques
        for i, plot in enumerate(self.liste_plot):
            plot.get_plot(axs[i], legend)

        # Ajuster la mise en page pour éviter le chevauchement
        plt.tight_layout()
        plt.show()


#class representing a multicurve plot
class MultiCurve :
    def __init__(self, titre, Y_label) :
        self.liste_curves = []
        self.title = titre
        self.Y_label = Y_label
        self.Ymin = 0
        self.Ymax = 1000

    def colorise_curves(self) :
        arcenciel = plt.cm.viridis(np.linspace(0, 1, len(self.liste_curves)))
        for i in range(0, len(self.liste_curves)) :
            if not self.liste_curves[i].colorfixed :
                self.liste_curves[i].color = arcenciel[i]

    def add_curve(self, new_curve) :
        self.liste_curves.append(new_curve)
        self.colorise_curves()
    
    def set_Yscale(self, ymin, ymax) :
        self.Ymin = ymin
        self.Ymax = ymax

    def get_plot(self, ax,  legend = True):
        # Ajout de chaque courbe
        for curve in self.liste_curves:
            ax.plot(curve.x, curve.y, linestyle=curve.linestyle, label=curve.curvename, color=curve.color, linewidth=0.5)
        
        # Fixer les limites de l'axe y
        ax.set_ylim(self.Ymin, self.Ymax)

        # Set label on axes
        ax.set_xlabel('Date')
        ax.set_ylabel(self.Y_label)

        # Set legend
        if legend :
            ax.legend(loc='lower left')

        # Set plot titre
        ax.set_title(self.title)

        # Formater les dates
        ax.xaxis.set_major_locator(mdates.DayLocator(interval=10))  # Intervalle de 10 jours
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))  # Format de la date

        # Rotation des labels pour éviter qu'ils se chevauchent
        plt.gcf().autofmt_xdate()

        ax.set_xlabel('date')


#class representing a multiplot object
class MultiPlot :
    def __init__(self) :
        self.liste_plot = []

    def add_plot(self, plot) :
        self.liste_plot.append(plot)

    def get_list_axe(self) :
        listaxe = []
        for plot in self.liste_plot :
            listaxe.append(plot.get_plot)

        return listaxe
    
    def make_plot(self, legend = True):
        # Calcul des dimensions de la grille (n lignes et 1 colonne ou plus, selon la préférence)
        n = len(self.liste_plot)
        rows = 1
        cols = n

        # Création de la figure et des sous-graphiques
        figure, axs = plt.subplots(rows, cols, figsize=(18, 9))
        if n == 1:
            axs = [axs]  # Pour uniformiser l'accès lorsque n = 1

        # Ajout de chaque plot aux sous-graphiques
        for i, plot in enumerate(self.liste_plot):
            plot.get_plot(axs[i], legend)

        # Ajuster la mise en page pour éviter le chevauchement
        plt.tight_layout()
        plt.show()
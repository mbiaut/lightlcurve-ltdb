from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle
import matplotlib.pyplot as plt
import numpy as np
import argparse
from array import array


# Plot path
#PLT_PATH = "/eos/home-e/etfortin/www/gbtx_reg"
PLT_PATH = "./test-ltdb"


# Function to get a list of tuples from db
def get_tuples_list_from_query(la_query, dbconnexion):
    current = dbconnexion.cursor()
    current.execute(la_query)
    result = current.fetchall()
    #desc = [row[0] for row in cur.description]
    current.close()
    return result


#class representing a ltdb fiber
class LtdbFiber :
    def __init__(self, ltdb, number, dbconnexion) :
        self.ltdb = ltdb
        self.number = number
        self.curve = curveOnPlot(self.ltdb + " fiber " + str(self.number))
        self.query_result = []
        self.dbconnexion = dbconnexion

    def get_point_from_DCS(self) :
        query = "SELECT to_char(valeurs.ts,'yyyy/mm/dd hh24:mi:ss'),valeurs.value_number FROM ATLAS_PVSSLAR.eventhistory valeurs, ATLAS_PVSSLAR.elements datapoint WHERE valeurs.element_id = datapoint.element_id AND regexp_like(datapoint.COMMENT_,'(.*)" + self.ltdb + " fiber " + str(self.number) + " lightpower(.*)') ORDER BY valeurs.ts ASC";
        self.query_result = get_tuples_list_from_query(query, self.dbconnexion)
        self.curve.assign_point(self.query_result)

    def filter_data(self) :
        filtered_result = []
        for date, value in self.query_result :
            #if 0 < value and value <= 3000 :
            if value <= 3000 :
                filtered_result.append((date, value))
        self.query_result = filtered_result
        self.curve.assign_point(self.query_result)

    def put_on_plot(self, plot) :
        plot.add_curve(self.curve)


#class representing a curve
class curveOnPlot :
    def __init__(self, curvename) :
        self.x = []
        self.y = []
        self.curvename = curvename
        self.color = "blue"

    def assign_point(self, tupleslist) :
        self.x = []
        self.y = []
        for untuple in tupleslist :
            self.x.append(datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp())
            self.y.append(untuple[1])


#class representing a multicurve plot
class multiCurvePlot :
    def __init__(self, Y_label, save = False) :
        self.liste_curves = []
        self.Y_label = Y_label
        self.save = save

    def colorise_curves(self) :
        arcenciel = plt.cm.viridis(np.linspace(0, 1, len(self.liste_curves)))
        for i in range(0, len(self.liste_curves)) :
            self.liste_curves[i].color = arcenciel[i]

    def add_curve(self, new_curve) :
        self.liste_curves.append(new_curve)
        self.colorise_curves()

    def make_plot(self) :
        # Create graph (for now arbitrary sized)
        plt.figure(figsize=(10, 6))

        # Add each curve
        for curve in self.liste_curves : 
            plt.plot(curve.x, curve.y, linestyle='-', label=curve.curvename, color=curve.color)

        # Add label and title
        plt.xlabel('date')
        plt.ylabel(self.Y_label)
        plt.title("Le super plot")

        # Formate date on X axis
        plt.gcf().autofmt_xdate()  # Auto rotation of the date

        if self.save :
            # Enregistrer le graphique en tant qu'image PNG
            #if not os.path.exists(PLT_PATH + "/REG_{}".format(reg)):
            #    os.makedirs(PLT_PATH + "/REG_{}".format(reg))

            plt.savefig(PLT_PATH +'graph.png')

            # Fermer le graphique pour passer au suivant
            plt.close()

        else :
            plt.show()


#function returning the list of ltdb fiber interesting us
def get_ltdb_fibers_list(dbconnexion) :
    liste_to_return = []
    for fibernum in range (1, 41) :
        liste_to_return.append(LtdbFiber("I02R", fibernum, dbconnexion))
        print(fibernum)
    return liste_to_return


def main():
    # Connection to the database
    try:
        dbconnexion = cx_Oracle.connect("ATLAS_PVSS_READER", "PVSSRED4PRO", "ATONR_ADG")
    except cx_Oracle.DatabaseError as e:
        print(f"Database connection error: {e}")
        return

    # Get list of fiber to watch
    ltdb_fibers_list = get_ltdb_fibers_list(dbconnexion)

    # Create the plot object
    le_plot = multiCurvePlot(Y_label = "lightpower (uW)", save = False);

    # Create all curves
    for fiber in ltdb_fibers_list :
        fiber.get_point_from_DCS()
        fiber.filter_data()
        fiber.put_on_plot(le_plot)

    # Finnaly print the plot
    le_plot.make_plot()

    # Close the database connection
    dbconnexion.close()

if __name__ == "__main__":
    main()
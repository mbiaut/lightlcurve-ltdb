from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle
import matplotlib.pyplot as plt
import numpy as np
import argparse
from array import array
import re
import mysql.connector

from lib_ltdb_light import *

def get_full_ltdb_barrel() :
    fullltdbname = ("H13L",
                "I12R",
                "H04R",
                "I15R",
                "H09R",
                "H11L",
                "I11L",
                "I11R",
                "I15L",
                "H01L",
                "H14L",
                "I05L",
                "I12L",
                "I02L",
                "I14L",
                "I14R",
                "I01R",
                "I07R",
                "I06L",
                "I04L",
                "I05R",
                "I03L",
                "I07L",
                "I06R",
                "I10L",
                "I03R",
                "I10R",
                "I09L",
                "H11R",
                "I02R",
                "I08R",
                "H05L",
                "I09R",
                "I01L",
                "H02L",
                "I13L",
                "I16L",
                "H05R",
                "H01R",
                "I13R",
                "I16R",
                "I08L",
                "H06L",
                "H06R",
                "H16L",
                "I04R",
                "H15L",
                "H15R",
                "H12R",
                "H14R",
                "H08L",
                "H16R",
                "H04L",
                "H12L",
                "H13R",
                "H07L",
                "H10L",
                "H08R",
                "H03R",
                "H07R",
                "H03L",
                "H10R",
                "H02R",
                "H09L")

    fullltdb = []
    for name in fullltdbname :
        fullltdb.append(Ltdb(name))

    return fullltdb


def get_full_ltdb_endcap() :
    fullltdbname = ("C03R",
                "A05R",
                "A05L",
                "C11L",
                "A13L",
                "C10L",
                "C08R",
                "A08L",
                "C10R",
                "C11R",
                "A10R",
                "C05R",
                "C01L",
                "A08R",
                "A07R",
                "A03R",
                "A01L",
                "C08L",
                "A11L",
                "A03L",
                "C03L",
                "C07L",
                "C13L",
                "C01R",
                "C07R",
                "A01R",
                "C13R",
                "C05L",
                "A07L",
                "A11R",
                "A13R",
                "A02Spe0",
                "A12Spe0",
                "A06Spe0",
                "C02Spe0",
                "C06Spe0",
                "C12Spe0",
                "A09Spe0",
                "C09Spe0",
                "C09Spe1",
                "A02Spe1",
                "C06Spe1",
                "A09Spe1",
                "C12Spe1",
                "A12Spe1",
                "C02Spe1",
                "A06Spe1",
                "A06HEC",
                "C09HEC",
                "C02HEC",
                "C06HEC",
                "A12HEC",
                "A09HEC",
                "C12HEC",
                "A02HEC",
                "A04F0",
                "C04F0",
                "A10L",
                "C04F1",
                "A04F1")
    fullltdb = []
    for name in fullltdbname :
        fullltdb.append(Ltdb(name))

    return fullltdb


#function returning the list of ltdb fiber interesting us
def get_ltdb_fibers_list(part = "ALL") :
    fullltdb = []
    if part == "ALL" :
        fullltdb = get_full_ltdb_barrel()
        fullltdb.append(get_full_ltdb_endcap())
    elif part == "BARREL" :
        fullltdb = get_full_ltdb_barrel()
    elif part == "ENDCAP" :
        fullltdb = get_full_ltdb_endcap()

    liste_fiber = []
    
    for ltdb in fullltdb :
        ltdb.add_all_mtx()
        for mtx in ltdb.concerned_mtx :
            liste_fiber.append(mtx.fiber1)
            liste_fiber.append(mtx.fiber2)
            print("fiber 1 : " + str(mtx.fiber1.ltdb) + " " + str(mtx.fiber1.number))
            print("fiber 2 : " + str(mtx.fiber2.ltdb) + " " + str(mtx.fiber2.number))

    return liste_fiber


def main():
    # Global variable for script :
    Y_plot_label = "lightpower (uW)"
    datedebut = '2023/01/01 00:00:00'
    datefinale = '2024/07/30 00:00:00'

    # Get list of fiber to watch
    ltdb_fibers_list_barrel = get_ltdb_fibers_list("BARREL")
    ltdb_fibers_list_endcap = get_ltdb_fibers_list("ENDCAP")

    # Create the two plot, barrel and endcap
    barrelplot = MultiCurve("Barrel exchanged mtx fiber", Y_label = Y_plot_label)
    endcapplot = MultiCurve("EndCap exchanged mtx fiber", Y_label = Y_plot_label)

    # Connection to the database
    try:
        dbconnexion = cx_Oracle.connect("ATLAS_PVSS_READER", "PVSSRED4PRO", "ATONR_ADG")
    except cx_Oracle.DatabaseError as e:
        print(f"Database connection error: {e}")
        return

    # Create all curves
    for fiber in ltdb_fibers_list_barrel :
        fiber.get_point(dbconnexion)
        if fiber.notempty() :
            fiber.filter_data()
            fiber.cut_at_date(datedebut)
            curvelabel = fiber.name+" : "+"{:.2f}".format(fiber.lightloss())
            if fiber.lightloss() < -5 or 5 < fiber.lightloss() : 
                newcurve = CurveOnPlot(curvelabel, fiber.get_daily_average_data(), linestyle='-')
                barrelplot.add_curve(newcurve)
                newcurve = CurveOnPlot(curvelabel, fiber.linear_regression(finaledate = datefinale), linestyle=':')
                barrelplot.add_curve(newcurve)
    
    for fiber in ltdb_fibers_list_endcap :
        fiber.get_point(dbconnexion)
        if fiber.notempty() :
            fiber.filter_data()
            fiber.cut_at_date(datedebut)
            curvelabel = fiber.name+" : "+"{:.2f}".format(fiber.lightloss())
            if fiber.lightloss() < -5 or 5 < fiber.lightloss() : 
                newcurve = CurveOnPlot(curvelabel, fiber.get_daily_average_data(), linestyle='-')
                endcapplot.add_curve(newcurve)
                newcurve = CurveOnPlot(curvelabel, fiber.linear_regression(finaledate = datefinale), linestyle=':')
                endcapplot.add_curve(newcurve)

    limitecurve = CurveOnPlot('', [(datedebut, 250), (datefinale, 250)], linestyle='--', color='red')
    barrelplot.add_curve(limitecurve)
    endcapplot.add_curve(limitecurve)

    # Close the database connection
    dbconnexion.close()
    
    # Set Y scale
    barrelplot.set_Yscale(0, 1000)
    endcapplot.set_Yscale(0, 1000)

    # Finnaly print the finale multiplot figure
    le_superplot = MultiPlot()
    le_superplot.add_plot(barrelplot)
    le_superplot.add_plot(endcapplot)

    le_superplot.make_plot( legend = False)

if __name__ == "__main__":
    main()
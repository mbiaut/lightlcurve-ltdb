#class representing a multiplot object
class MultiPlot :
    def __init__(self) :
        self.liste_plot = []

    def add_plot(self, plot) :
        self.liste_plot.append(plot)

    def get_list_axe(self) :
        listaxe = []
        for plot in self.liste_plot :
            listaxe.append(plot.get_plot)

        return listaxe

    def make_plot(self) :
        # Calcul des dimensions de la grille (n lignes et 1 colonne ou plus, selon la préférence)
        rows = 1
        cols = len(self.liste_plot)
        n = len(self.liste_plot)

        # Création de la figure et des sous-graphiques
        axes = self.get_list_axe()


        # Ajuster la mise en page pour éviter le chevauchement
        plt.tight_layout()
        plt.show()

#class representing a multicurve plot
class MultiCurve :
    def __init__(self, titre, Y_label) :
        self.liste_curves = []
        self.Y_label = Y_label
        self.Ymin = 0
        self.Ymax = 1000

    def add_curve(self, new_curve) :
        self.liste_curves.append(new_curve)
    
    def set_Yscale(self, ymin, ymax) :
        self.Ymin = ymin
        self.Ymax = ymax

    def get_plot(self) :
        axe = plt.subplots(rows, cols, figsize=(18, 9))

        # Add each curve
        for curve in self.liste_curves_multiplot[i] : 
            axe.plot(curve.x, curve.y, linestyle='-', label=curve.curvename, color=curve.color, linewidth=0.5)
            axe.set_xlabel('date')
            axe.set_ylabel(self.Y_label)
            axe.legend()
            axe.set_ylim(self.Ymin, self.Ymax)

            axe[i].set_title(self.title)

        return axe

    def get_plot(self, ax):
        # Ajout de chaque courbe
        for curve in self.liste_curves:
            ax.plot(curve.x, curve.y, linestyle='-', label=curve.curvename, color=curve.color, linewidth=0.5)
        
        ax.set_xlabel('date')
        ax.set_ylabel(self.Y_label)
        ax.legend()
        ax.set_ylim(self.Ymin, self.Ymax)
        ax.set_title(self.title)
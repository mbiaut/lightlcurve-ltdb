from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle
import matplotlib.pyplot as plt
import numpy as np
import argparse
from array import array
import re
import mysql.connector

from lib_ltdb_light import *

def get_list_mtx_echanged(part = "ALL") :
    # Get topologie information
    topo_connection = mysql.connector.connect(
        user = 'reader',
        password = 'monUser',
        host = 'dbod-eti-lar-mon.cern.ch',
        database = 'LTDB_TOPO',
        port = 5505
    )
    cursor = topo_connection.cursor()

    # Exemple d'exécution d'une requête SQL
    query = "SELECT ltdbName, mtxIds FROM MTx_Exchange ORDER BY date_of_exchange DESC;"
    if part == "ALL" :
        query = "SELECT ltdbName, mtxIds FROM MTx_Exchange ORDER BY date_of_exchange DESC;"
    elif part == "BARREL" :
        query = "SELECT ltdbName, mtxIds FROM MTx_Exchange WHERE ltdbName LIKE 'I%' OR ltdbName LIKE 'H%' ORDER BY date_of_exchange DESC;"
    elif part == "ENDCAP" :
        query = "SELECT ltdbName, mtxIds FROM MTx_Exchange WHERE ltdbName NOT LIKE 'I%' AND ltdbName NOT LIKE 'H%' ORDER BY date_of_exchange DESC;"
    cursor.execute(query)
    results = cursor.fetchall()

    mtx_exchanged = []
    for row in results:
        mtx_exchanged.append(row)

    # Fermeture de la connexion
    cursor.close()
    topo_connection.close()

    return mtx_exchanged


#function returning the list of ltdb fiber interesting us
def get_ltdb_fibers_list(part = "ALL") :
    mtx_exchanged = get_list_mtx_echanged(part)

    list_mtx_obj = []
    for mtx in mtx_exchanged :
        list_mtx_obj.append(LtdbMtx(mtx[0], mtx[1]))

    liste_to_return = []
    for mtxobj in list_mtx_obj :
        liste_to_return.append(mtxobj.fiber1)
        liste_to_return.append(mtxobj.fiber2)

    return liste_to_return


def main():
    # Get list of fiber to watch
    ltdb_fibers_list_barrel = get_ltdb_fibers_list("BARREL")
    ltdb_fibers_list_endcap = get_ltdb_fibers_list("ENDCAP")

    # Define tolerate loss 
    tolerateloss_barrel = -10
    tolerateloss_endcap = -5

    # Variable to get lightloss average
    lightloss_average_barrel = 0
    lightloss_average_endcap = 0

    # Create the two plot, barrel and endcap
    barrelplot = MultiCurve("Barrel exchanged mtx fiber (only"+str(tolerateloss_barrel)+"%)", Y_label = "relative loss (%)")
    endcapplot = MultiCurve("EndCap exchanged mtx fiber (only"+str(tolerateloss_endcap)+"%)", Y_label = "relative loss (%)")

    # Connection to the database
    try:
        dbconnexion = cx_Oracle.connect("ATLAS_PVSS_READER", "PVSSRED4PRO", "ATONR_ADG")
    except cx_Oracle.DatabaseError as e:
        print(f"Database connection error: {e}")
        return

    # Create all curves
    nbfiber = 0
    for fiber in ltdb_fibers_list_barrel :
        fiber.get_point(dbconnexion)
        if fiber.notempty() :
            fiber.filter_data()
            fiber.cut_at_first_march24()
            lightloss_average_barrel += fiber.lightloss()
            nbfiber += 1
            if fiber.lightloss() < tolerateloss_barrel :
                curvelabel = fiber.name+" : "+"{:.2f}".format(fiber.lightloss())
                newcurve = CurveOnPlot(curvelabel, fiber.get_relativepert_dailymoyenned_data())
                #barrelplot.add_curve(newcurve)
                newcurve = CurveOnPlot(curvelabel, fiber.get_daily_lightloss_data())
                barrelplot.add_curve(newcurve)
    lightloss_average_barrel = lightloss_average_barrel/nbfiber
    
    nbfiber = 0
    for fiber in ltdb_fibers_list_endcap :
        fiber.get_point(dbconnexion)
        if fiber.notempty() :
            fiber.filter_data()
            fiber.cut_at_first_march24()
            lightloss_average_endcap += fiber.lightloss()
            nbfiber += 1
            if fiber.lightloss() < tolerateloss_endcap :
                curvelabel = fiber.name+" : "+"{:.2f}".format(fiber.lightloss())
                newcurve = CurveOnPlot(curvelabel, fiber.get_relativepert_dailymoyenned_data())
                #endcapplot.add_curve(newcurve)
                newcurve = CurveOnPlot(curvelabel, fiber.get_daily_lightloss_data())
                endcapplot.add_curve(newcurve)
    lightloss_average_endcap = lightloss_average_endcap/nbfiber

    lesdonnecroise = []
    lesdonnecroise.append(('2024/03/01 00:00:00', -15))
    lesdonnecroise.append(('2024/03/15 00:00:00', -5))

    lesdonnecroise.append(('2024/05/15 00:00:00', -5))

    lesdonnecroise.append(('2024/04/01 00:00:00', -15))
    lesdonnecroise.append(('2024/04/15 00:00:00', -5))
    lesdonnecroise.append(('2024/05/01 00:00:00', -15))
    
    lesdonnecroise.append(('2024/06/01 00:00:00', -15))
    newcurve = CurveOnPlot(curvelabel, lesdonnecroise)
    endcapplot.add_curve(newcurve)

    # Print average light loss
    print("Average lightloss for barrel = " + str(lightloss_average_barrel))
    print("Average lightloss for endcap = " + str(lightloss_average_endcap))

    # Close the database connection
    dbconnexion.close()
    
    # Set Y scale
    barrelplot.set_Yscale(-20, 3)
    endcapplot.set_Yscale(-20, 3)

    # Finnaly print the finale multiplot figure
    le_superplot = MultiPlot()
    le_superplot.add_plot(barrelplot)
    le_superplot.add_plot(endcapplot)

    le_superplot.make_plot()

if __name__ == "__main__":
    main()
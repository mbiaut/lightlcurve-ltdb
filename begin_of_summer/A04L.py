from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle
import matplotlib.pyplot as plt
import numpy as np
import argparse
from array import array
import re
import mysql.connector

from lib_ltdb_light import *

def get_list_mtx_echanged(part = "ALL") :
    # Get topologie information
    topo_connection = mysql.connector.connect(
        user = 'reader',
        password = 'monUser',
        host = 'dbod-eti-lar-mon.cern.ch',
        database = 'LTDB_TOPO',
        port = 5505
    )
    cursor = topo_connection.cursor()

    # Exemple d'exécution d'une requête SQL
    query = "SELECT ltdbName, mtxIds FROM MTx_Exchange ORDER BY date_of_exchange DESC;"
    if part == "ALL" :
        query = "SELECT ltdbName, mtxIds FROM MTx_Exchange ORDER BY date_of_exchange DESC;"
    elif part == "BARREL" :
        query = "SELECT ltdbName, mtxIds FROM MTx_Exchange WHERE ltdbName LIKE 'I%' OR ltdbName LIKE 'H%' ORDER BY date_of_exchange DESC;"
    elif part == "ENDCAP" :
        query = "SELECT ltdbName, mtxIds FROM MTx_Exchange WHERE ltdbName NOT LIKE 'I%' AND ltdbName NOT LIKE 'H%' ORDER BY date_of_exchange DESC;"
    cursor.execute(query)
    results = cursor.fetchall()

    mtx_exchanged = []
    for row in results:
        mtx_exchanged.append(row)

    # Fermeture de la connexion
    cursor.close()
    topo_connection.close()

    return mtx_exchanged


#function returning the list of ltdb fiber interesting us
def get_ltdb_fibers_list(part = "ALL") :
    mtx_exchanged = []
    for i in range(1, 20) :
        mtx_exchanged.append(("A04L", i))

    list_mtx_obj = []
    for mtx in mtx_exchanged :
        list_mtx_obj.append(LtdbMtx(mtx[0], mtx[1]))

    liste_to_return = []
    for mtxobj in list_mtx_obj :
        liste_to_return.append(mtxobj.fiber1)
        liste_to_return.append(mtxobj.fiber2)

    return liste_to_return


def main():
    # Get list of fiber to watch
    ltdb_fibers_list_barrel = get_ltdb_fibers_list()

    # Create the two plot, barrel and endcap
    barrelplot = MultiCurve("A04L fiber (only-5%)", Y_label = "relative loss (%)")
  
    tolerateloss = 0

    # Connection to the database
    try:
        dbconnexion = cx_Oracle.connect("ATLAS_PVSS_READER", "PVSSRED4PRO", "ATONR_ADG")
    except cx_Oracle.DatabaseError as e:
        print(f"Database connection error: {e}")
        return

    # Create all curves
    for fiber in ltdb_fibers_list_barrel :
        fiber.get_point(dbconnexion)
        if fiber.notempty() :
            fiber.filter_data()
            if fiber.lightloss() < tolerateloss :
                newcurve = CurveOnPlot(fiber.name, fiber.cooked_data)
                barrelplot.add_curve(newcurve)


    # Close the database connection
    dbconnexion.close()
    
    # Set Y scale
    barrelplot.set_Yscale(0, 1000)

    # Finnaly print the finale multiplot figure
    le_superplot = MultiPlot()
    le_superplot.add_plot(barrelplot)

    le_superplot.make_plot()

if __name__ == "__main__":
    main()
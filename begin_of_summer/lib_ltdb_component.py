from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle
import matplotlib.pyplot as plt
import numpy as np
import argparse
import pickle
import matplotlib.dates as mdates
from array import array	
from scipy import stats


# Function to get a list of tuples from db
def get_tuples_list_from_query(la_query, dbconnexion):
    current = dbconnexion.cursor()
    current.execute(la_query)
    result = current.fetchall()
    #desc = [row[0] for row in cur.description]
    current.close()
    return result

#class representig a ltdb
class Ltdb :
    def __init__(self, name) :
        self.name = self.adaptltdbname(name)
        self.changed_mtx = []
        self.concerned_mtx = []

    def add_changed_mtx(self, mtxnumber) :
        self.changed_mtx.append(ltdbMtx(self.name, mtxnumber))

    def add_all_mtx(self) :
        for mtxnumber in range(1, 20) :
            self.concerned_mtx.append(LtdbMtx(self.name, mtxnumber))

    def adaptltdbname(self, name) :
        name = name.replace("Spe0", "L")
        name = name.replace("Spe1", "R")
        name = name.replace("F1", "L")
        name = name.replace("F0", "R")
        name = name.replace("HEC", "H")
        return name


#class representig a ltdb mtx
class LtdbMtx :
    def __init__(self, ltdb, number) :
        self.ltdb = self.adaptltdbname(ltdb)
        self.number = number
        self.fiber1 = LtdbFiber(self.ltdb, number*2-1)
        self.fiber2 = LtdbFiber(self.ltdb, number*2)

    def adaptltdbname(self, name) :
        name = name.replace("Spe0", "L")
        name = name.replace("Spe1", "R")
        name = name.replace("F1", "L")
        name = name.replace("F0", "R")
        name = name.replace("HEC", "H")
        return name

#class representing a ltdb fiber
class LtdbFiber :
    def __init__(self, ltdb, number) :
        self.ltdb = self.adaptltdbname(ltdb)
        self.number = number
        self.name = ltdb + "_fiber_" + str(number)
        self.raw_data = []
        self.cooked_data = []
        self.comment = ""

    def adaptltdbname(self, name) :
        name = name.replace("Spe0", "L")
        name = name.replace("Spe1", "R")
        name = name.replace("F1", "L")
        name = name.replace("F0", "R")
        name = name.replace("HEC", "H")
        return name

    def get_point(self, dbconnexion, force_DCS = False) :
        filename = "./savedpoints/" + self.ltdb + "_fiber_" + str(self.number) + ".pkl"
        file_exist = os.path.exists(filename)
        
        if file_exist and not force_DCS :
            self.get_point_pickle()

        else :
            self.get_point_from_DCS(dbconnexion)
            self.save_point_pickle()

        if self.raw_data == [] :
            print("WARNING : " + self.ltdb + " fiber " + str(self.number) + " doesn't have any point, will not be printed")

        self.cooked_data = self.raw_data 

    def save_point_pickle(self) :
        filename = "./savedpoints/" + self.ltdb + "_fiber_" + str(self.number) + ".pkl"
        # Ouvrir un fichier en mode binaire pour écrire
        with open(filename, 'wb') as savfile:
            # Sérialiser l'objet et l'écrire dans le fichier
            pickle.dump(self.raw_data, savfile)

    def get_point_pickle(self) :
        print("get point from pickle : " + self.ltdb + " " + str(self.number))
        filename = "./savedpoints/" + self.ltdb + "_fiber_" + str(self.number) + ".pkl"
        # Ouvrir le fichier en mode binaire pour lire
        with open(filename, 'rb') as savfile:
            # Lire le contenu du fichier et désérialiser l'objet
            self.raw_data = pickle.load(savfile)

    def get_point_from_DCS(self, dbconnexion) :
        print("get point from DCS : " + self.ltdb + " " + str(self.number))
        query = "SELECT to_char(valeurs.ts,'yyyy/mm/dd hh24:mi:ss'),valeurs.value_number FROM ATLAS_PVSSLAR.eventhistory valeurs, ATLAS_PVSSLAR.elements datapoint WHERE valeurs.element_id = datapoint.element_id AND regexp_like(datapoint.COMMENT_,'(.*)" + self.ltdb + " fiber " + str(self.number) + " lightpower(.*)') ORDER BY valeurs.ts ASC";
        print("interrogation : "+self.ltdb + " fiber " + str(self.number))
        self.raw_data = get_tuples_list_from_query(query, dbconnexion)

    def cut_at_first_march24(self) :
        current_data = self.cooked_data
        self.cooked_data = []

        datedebut = datetime.strptime('2024/03/01 00:00:00', '%Y/%m/%d %H:%M:%S').timestamp()
        for untuple in current_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            if datedebut < date :
                self.cooked_data.append(untuple)
        
        return self.cooked_data

    def cut_at_20_april24(self) :
        current_data = self.cooked_data
        self.cooked_data = []

        datedebut = datetime.strptime('2024/04/20 00:00:00', '%Y/%m/%d %H:%M:%S').timestamp()
        for untuple in current_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            if datedebut < date :
                self.cooked_data.append(untuple)
        
        return self.cooked_data

    def cut_at_date(self, datedebutstr) :
        current_data = self.cooked_data
        self.cooked_data = []

        datedebut = datetime.strptime(datedebutstr, '%Y/%m/%d %H:%M:%S').timestamp()
        for untuple in current_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            if datedebut < date :
                self.cooked_data.append(untuple)
        
        return self.cooked_data

    def filter_data(self) :
        filtered_result = []
        for date, value in self.cooked_data :
            if 0 < value and value <= 3000 :
                filtered_result.append((date, value))
        self.cooked_data = filtered_result

        return self.cooked_data

    def get_relativepertintegral_data(self) :
        relativepert_points = []

        lightat20febr24 = self.cooked_data[0][1];
        integrallight = 0;
        i = 0
        for untuple in self.cooked_data :     
            if 0 < i :
                integrallight = integrallight + (untuple[1]-lightat20febr24)
                currentmoyenne = integrallight/i
                currentrelativepert = currentmoyenne/lightat20febr24*100
                relativepert_points.append((untuple[0], currentrelativepert))
            else :
                relativepert_points.append((untuple[0], 0))
            i = i + 1
        
        return relativepert_points

    def get_relativepert_data(self) :
        relativepert_points = []

        lightat20febr24 = self.cooked_data[0][1];
        integrallight = 0;
        for untuple in self.user_asked_data :
            if lightat20febr24 == 0 :
                lightat20febr24 = untuple[1]
            currentpert = (untuple[1]-lightat20febr24)
            currentrelativepert = currentpert/lightat20febr24*100
            relativepert_points.append((untuple[0], currentrelativepert))
        
        return relativepert_points

    
    def get_relativepert_data(self) :
        relativepert_points = []

        lightat20febr24 = self.cooked_data[0][1];
        integrallight = 0;
        for untuple in self.cooked_data :
            currentpert = (untuple[1]-lightat20febr24)
            currentrelativepert = currentpert/lightat20febr24*100
            relativepert_points.append((untuple[0], currentrelativepert))
        
        return relativepert_points

    def get_relativepert_dailymoyenned_data(self) :
        relativepert_dailymoyenned_points = []

        firstlight = self.cooked_data[0][1]
        firstindice = 0
        firstimestamp = datetime.strptime(self.cooked_data[0][0], '%Y/%m/%d %H:%M:%S').timestamp()
        currentimestamp = datetime.strptime(self.cooked_data[0][0], '%Y/%m/%d %H:%M:%S').timestamp()
                    
        currentmoyenne = 0
        j = 0
        # Moyenne by day
        for untuple in self.cooked_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            currentmoyenne += untuple[1] - firstlight
            j += 1
            if (currentimestamp + 3600*24) < date :
                moyenne = currentmoyenne/j/firstlight*100
                currentmoyenne = 0
                j = 0
                currentimestamp = date
                relativepert_dailymoyenned_points.append((untuple[0], moyenne))
        
        return relativepert_dailymoyenned_points

    def get_dailymoyenned_data(self) :
        relativepert_dailymoyenned_points = []

        firstlight = self.cooked_data[0][1]
        firstindice = 0
        firstimestamp = datetime.strptime(self.cooked_data[0][0], '%Y/%m/%d %H:%M:%S').timestamp()
        currentimestamp = datetime.strptime(self.cooked_data[0][0], '%Y/%m/%d %H:%M:%S').timestamp()
                    
        currentmoyenne = 0
        j = 0
        # Moyenne by day
        for untuple in self.cooked_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            currentmoyenne += untuple[1]
            j += 1
            if (currentimestamp + 3600*24) < date :
                moyenne = currentmoyenne/j
                currentmoyenne = 0
                j = 0
                currentimestamp = date
                relativepert_dailymoyenned_points.append((untuple[0], moyenne))
        
        return relativepert_dailymoyenned_points

    def get_daily_dico(self) :
        daily_dico = {}
        for untuple in self.cooked_data :
            theday = untuple[0].split(' ')[0]
            if theday not in daily_dico :
                daily_dico[theday] = []
            daily_dico[theday].append(untuple[1])

        return daily_dico

    def get_daily_average_data(self) :
        daily_dico = self.get_daily_dico()
        daily_average_data = []

        for day in daily_dico :
            daily_average = 0
            for val in daily_dico[day] :
                daily_average += val
            daily_average = daily_average / len(daily_dico[day])
            date  = day + ' 00:00:00'
            daily_average_data.append((date, daily_average))

        return daily_average_data

    def get_last_day_average(self) :
        daily_average_data = self.get_daily_average_data()
        return daily_average_data[len(daily_average_data)-1][1]

    def get_daily_lightloss_data(self) :
        firstlight = self.cooked_data[0][1]
        daily_average_data = self.get_daily_average_data()
        daily_lightloss_data = []
        for untuple in daily_average_data :
            currentlightloss = (untuple[1]-firstlight)/firstlight*100
            daily_lightloss_data.append((untuple[0], currentlightloss))

        return daily_lightloss_data

    def lightloss_integral(self) :
        datedebut = datetime.strptime('2024/03/01 00:00:00', '%Y/%m/%d %H:%M:%S').timestamp()
        lightat20febr24 = 0;
        integrallight = 0;
        i = 0
        for untuple in self.cooked_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            if datedebut < date :
                if lightat20febr24 == 0 :
                    lightat20febr24 = untuple[1]
                integrallight = integrallight + (untuple[1]-lightat20febr24)
                i += 1

        moyenne = integrallight/i
        relativepert = moyenne/lightat20febr24*100

        return relativepert
  
    def lightloss(self) :
        relativepert_dailymoyenned_points = []

        datedebut = datetime.strptime('2024/03/01 00:00:00', '%Y/%m/%d %H:%M:%S').timestamp()
        firstlight = 0;
        firstindice = 0;
        firstimestamp = 0;
        currentimestamp = 0;

        # Find first point
        i = 0
        while firstlight == 0 :
            date = datetime.strptime(self.cooked_data[i][0], '%Y/%m/%d %H:%M:%S').timestamp()
            if datedebut < date :
                firstlight = self.cooked_data[i][1];
                firstindice = i
                firstimestamp = date
                currentimestamp = date
            i += 1
                    
        currentmoyenne = 0
        j = 0
        # Moyenne by day
        for untuple in self.cooked_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            if firstimestamp < date :
                currentmoyenne += untuple[1] - firstlight
                j += 1
                if (currentimestamp + 3600*24) < date :
                    moyenne = currentmoyenne/j/firstlight*100
                    currentmoyenne = 0
                    j = 0
                    currentimestamp = date
                    relativepert_dailymoyenned_points.append((untuple[0], moyenne))
        
        return relativepert_dailymoyenned_points[len(relativepert_dailymoyenned_points)-1][1]
            
    def notempty(self) :
        if self.raw_data == [] :
            return False
        else :
            return True

    def linear_regression(self, finaledate = "notspecified") :
        if finaledate == "notspecified" :
            finaledate = self.cooked_data[len(self.cooked_data)-1][0]

        x = []
        y = []
        datasorted = []

        for untuple in self.cooked_data :
            datasorted.append(((datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()), untuple[1]))
        datasorted = sorted(datasorted, key=lambda x: x[0])
        for untuple in datasorted :
            x.append(untuple[0])
            y.append(untuple[1])

        a, b, r_value, p_value, std_err = stats.linregress(x, y)

        droite = []
        x1 = datetime.strptime(self.cooked_data[0][0], '%Y/%m/%d %H:%M:%S').timestamp()
        x2 = datetime.strptime(finaledate, '%Y/%m/%d %H:%M:%S').timestamp()
        y1 = a*x1 + b
        y2 = a*x2 + b
        droite = [
            (self.cooked_data[0][0], y1), 
            (finaledate, y2)
            ]

        return droite
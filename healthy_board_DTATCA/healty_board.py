from datetime import datetime,timedelta
import time
import sys
import os
import argparse
import cx_Oracle
import matplotlib.pyplot as plt
import numpy as np
import argparse
from array import array
import re
import mysql.connector

parent_dir = os.path.dirname(os.path.dirname(__file__))
lib_dir = os.path.join(parent_dir, 'lib')
sys.path.append(lib_dir)
from lib_plot import *


# Function to get a list of tuples from db
def get_tuples_list_from_query(la_query, dbconnexion):
    current = dbconnexion.cursor()
    current.execute(la_query)
    result = current.fetchall()
    #desc = [row[0] for row in cur.description]
    current.close()
    return result


def get_point_from_DCS(dbconnexion, shelf, board) :
    print("get point from DCS")
    laregex = "*LAR DTATCA Shelf{} Board{} healthy*".format(shelf, board)
    query = "SELECT to_char(valeurs.ts,'yyyy/mm/dd hh24:mi:ss'), valeurs.value_number FROM ATLAS_PVSSLAR.eventhistory valeurs, ATLAS_PVSSLAR.elements datapoint WHERE valeurs.element_id = datapoint.element_id AND regexp_like(datapoint.COMMENT_, '" + laregex + "') ORDER BY valeurs.ts ASC";
    # query = "SELECT to_char(valeurs.ts,'yyyy/mm/dd hh24:mi:ss'), valeurs.value_number FROM ATLAS_PVSSLAR.eventhistory valeurs, ATLAS_PVSSLAR.elements datapoint WHERE valeurs.element_id = datapoint.element_id AND regexp_like(datapoint.COMMENT_, '') ORDER BY valeurs.ts ASC";
    raw_data = get_tuples_list_from_query(query, dbconnexion)
    return raw_data


def cut_at_date(current_data, datedebutstr) :
        cooked_data = []

        datedebut = datetime.strptime(datedebutstr, '%Y/%m/%d %H:%M:%S').timestamp()
        for untuple in current_data :
            date = datetime.strptime(untuple[0], '%Y/%m/%d %H:%M:%S').timestamp()
            if datedebut < date :
                cooked_data.append(untuple)

        return(cooked_data)


def main():
    print("djobi")
    # Global variable for script :
    Y_plot_label = ""
    datedebut = '2024/9/01 00:00:00'
    datefinale = '2024/12/31 00:00:00'

    # Create the two plot, barrel and endcap
    shelf1plot = MultiCurve("Shelf 1 unhealthy board", Y_label = Y_plot_label, Ymin = -0, Ymax = 1)
    shelf2plot = MultiCurve("Shelf 2 unhealthy board", Y_label = Y_plot_label, Ymin = -0, Ymax = 1)
    shelf3plot = MultiCurve("Shelf 3 unhealthy board", Y_label = Y_plot_label, Ymin = -0, Ymax = 1)

    # Connection to the database
    try:
        dbconnexion = cx_Oracle.connect("ATLAS_PVSS_READER", "PVSSRED4PRO", "ATONR_ADG")
    except cx_Oracle.DatabaseError as e:
        print(f"Database connection error: {e}")
        return
    print("djobar")

    # Create all curves
    # Shelf 1
    shelf = 1
    boardshelf1 = [3, 4, 5, 6, 9, 10, 11, 12, 13 ,14]
    for board in boardshelf1 :
        mesdata = get_point_from_DCS(dbconnexion, shelf, board)
        mesdatacuted = cut_at_date(mesdata, datedebut)
        curvelabel = "shelf{} board{}".format(shelf, board)
        newcurve = CurveOnPlot(curvelabel, mesdatacuted, linestyle='-')
        shelf1plot.add_curve(newcurve)
        print("djobu")

    # Shelf 2
    shelf = 2
    boardshelf2 = [3, 4, 5, 6, 9, 10, 11, 12, 13 ,14]
    for board in boardshelf2 :
        mesdata = get_point_from_DCS(dbconnexion, shelf, board)
        mesdatacuted = cut_at_date(mesdata, datedebut)
        curvelabel = "shelf{} board{}".format(shelf, board)
        newcurve = CurveOnPlot(curvelabel, mesdatacuted, linestyle='-')
        shelf2plot.add_curve(newcurve)
        print("djobu")

    # Shelf 3
    shelf = 3
    boardshelf3 = [2, 3, 4, 5, 6, 9, 10, 11, 12, 13]
    for board in boardshelf3 :
        mesdata = get_point_from_DCS(dbconnexion, shelf, board)
        mesdatacuted = cut_at_date(mesdata, datedebut)
        curvelabel = "shelf{} board{}".format(shelf, board)
        newcurve = CurveOnPlot(curvelabel, mesdatacuted, linestyle='-')
        shelf3plot.add_curve(newcurve)
        print("djobu")
        
    print("djobyh")
    # Finnaly print the finale multiplot figure
    le_superplot = MultiPlot()
    le_superplot.add_plot(shelf1plot)
    le_superplot.add_plot(shelf2plot)
    le_superplot.add_plot(shelf3plot)

    print("superplot creation")
    le_superplot.make_plot(legend = False, bar = True)
    print("djoba")


if __name__ == "__main__":
    main()